<?php
namespace frontend\modules\leave\components;
use frontend\modules\leave\models\Notify;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class DataLoad extends \yii\base\Component {
    public $countNotice;
    public function loadDataNotify($limit)
    {
        $noti = new Notify();
            $arr=  $noti->getDataLimited($limit);
         $this->countNotice = $noti->getCountNoticeUser();
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' =>  $arr,
                'pagination' => [
                    'pageSize' => 1000,
                ],
            ]);
               
            return $dataProvider;
    }
}

