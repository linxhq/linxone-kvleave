<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


return [
    //home
    'Home' => 'Trang chủ',
    'Add a leave request' => 'Viết xin phép nghỉ',
    'Sign up' => 'Đăng ký',
    'Sign in' => 'Đăng nhập',
    'Change language' => 'Thay đổi ngôn ngữ',
    'Close Menu' => 'Đóng',
    'English' => 'Tiếng anh',
    'Vietnamese' => 'Tiếng Việt',
    'Logout' => 'Đăng xuất',
    //login
    'password' => 'mật khẩu',
    'Incorrect username or password.' => 'Tên người dùng hoặc mật khẩu không đúng.',
    'Password' => 'Mật khẩu',
    'Remember password' => 'Nhớ tài khoản',
    'Forgot the password?' => 'Quên mật khẩu',
    // sign up
    'Username' => 'Tên người dùng',
    'Re-Password' => 'Xác nhận mật khẩu',
    // leave content
    'send' => 'gửi',
    'send to' => 'gửi tới',
    'Send' => 'Gửi',
    'to' => 'tới',
    'Send to' => 'Gửi tới',
    'Start from:' => 'Bắt đầu từ:',
    'Sick leave' => 'Xin nghỉ ốm',
    'Maternity leave' => 'Xin nghỉ thai sản',
    'Be on leave' => 'Xin nghỉ phép',
    'Unpaid leave' => 'Xin nghỉ không lương',
    'Leave type' => 'Chọn loại nghỉ phép',
    'Start time' => 'Thời gian bắt đầu',
    'End time' => 'Thời gian kết thúc',
    'Notes' => 'Ghi chú',
    'Status' => 'Trạng thái',
    'Approved' => 'Đã chấp nhận',
    'Rejected' => 'Đã từ chối',
    'In Progress' => 'Đang chờ xử lý',
    'Manage' => 'Quản lý',
    'Statistic' => 'Thống kê',
    'Add request'=>'Viết nghỉ phép',
    'In progress'=>'Chờ xử lý',
    'Approved'=>'Đã chấp nhận',
    'Rejected'=>'Đã từ chối',
    'Accept'=>'Chấp nhận',
    'Reject'=>'Từ chối',
    'Export to Excel'=>'Xuất ra Excel',
    'No notice'=>'Không có thông báo.',
    'See all'=>'Xem tất cả',
    'Others'=>'Người khác',
    'All'=>'Tất cả',
    'Me'=>'Tôi',
    'Choose a person'=>'Chọn một người',
];
