<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use frontend\modules\leave\models\User;
use frontend\assets\AppAsset;
use dosamigos\selectize\SelectizeTextInput;
use wbraganca\tagsinput\TagsinputWidget;
use yii\web\View;
use yii\web\Application;

$options = ['class' => 'form-control'];
$arr = User::getAllUsers();
$sendToid;
?>




<div class="panel panel-primary">
    <div class="panel-heading" ><h3><?= Yii::t('frontend', 'Add a leave request') ?></h3></div>
    <div class="panel-body" style="background: #f4f0ec;padding: 50px">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="<?= Yii::$app->request->baseUrl ?>/js/leave/tokenize2.js"></script>

        <div class="taginput-send">
            <label class="control-label" id="send-to-label"><?= Yii::t('frontend', 'Send to') ?></label>

            <select id ="sendto" size="1" class="email" name="sendto" multiple>
                <?php
                foreach ($arr as $item) {
                    echo '<option value=' . $item->attributes['id'] . '>' . $item->attributes['email'] . '</option>';
                }
                ?>

            </select>

            <div class="help-block" id="emailField">Email cannot be blank.</div>

        </div>
        <div class="taginput-cc">

            <a href="javascript:void(0)" id="click-cc">   <label class="control-label">cc</label></a>

            <select id="cc"  class="email"  multiple>
                <?php
                foreach ($arr as $item) {
                    echo '<option value=' . $item->attributes['id'] . '>' . $item->attributes['email'] . '</option>';
                }
                ?>
            </select>

        </div>

        <script>$('#sendto').tokenize2();</script>
        <script>$('#cc').tokenize2();</script>

        <?php
        $form = ActiveForm::begin([
                    'action' => ['create'],
                    'options' => [
                        'id' => 'requestForm'
                    ]
        ]);
        ?>



        <?=
                $form->field($model, "type")
                ->dropDownList(['1' => Yii::t('frontend', 'Sick leave'),
                    '2' => Yii::t('frontend', 'Unpaid leave'),
                    '3' => Yii::t('frontend', 'Be on leave'),
                    '4' => Yii::t('frontend', 'Maternity leave')])
                ->label(Yii::t('frontend', 'Leave type'))
        ?>

        <?=
        $form->field($model, "startTime")->widget(DateTimePicker::className(), [
            'name' => 'startTime',
            'options' => [
                'placeholder' => 'Start time...',
                'class' => 'picker',
                'id' => 'startTime',
                'style' =>
                [
                    'z-index' => '0'
                ],
            ],
            'pluginOptions' => ['autoclose' => true, 'format' => 'd-mm-yyyy HH:ii P',]
        ])->label(Yii::t('frontend', 'Start time'))
        ?>
        <div class="help-block" id="startTimeField">Start time can't is Saturday or Sunday.</div>
        <?=
        $form->field($model, "endTime")->widget(DateTimePicker::className(), [
            'name' => 'startTime',
            'options' => [
                'placeholder' => 'End time...',
                'class' => 'picker',
                'id' => 'endTime',
                'style' =>
                [
                    'z-index' => '0'
                ],
            ],
            'pluginOptions' => ['autoclose' => true, 'format' => 'd-mm-yyyy HH:ii P',]
        ])->label(Yii::t('frontend', 'End time'))
        ?>
        <div class="help-block" id="endTimeField">Start time can't is Saturday or Sunday.</div>
<?= $form->field($model, 'note')->textarea()->label(Yii::t('frontend', 'Notes')) ?>



        <div class="form-group">
<?= Html::submitButton(Yii::t('frontend', 'Send'), ['class' => 'btn btn-primary', 'id' => 'btn','value'=> Yii::$app->request->baseUrl]) ?>
        </div>

<?php ActiveForm::end(); ?>


    </div>
    

</div>




