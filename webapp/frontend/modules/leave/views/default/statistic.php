
<?php

use yii2fullcalendar\yii2fullcalendar;
use yii\web\JsExpression;

$user = new frontend\modules\leave\models\User;
$arr = $user->find()->asArray()->all();
?>
<div class="calendar-container">
    <div class="calendar-header" style=" margin-Top: 100px; ">
        <div>
            <select id="select-all">
                <option value="me" selected><?= Yii::t('frontend', 'Me') ?></option>
                <option value="other"><?= Yii::t('frontend', 'Others') ?></option>
                <option value="all"><?= Yii::t('frontend', 'All') ?></option>

            </select>
            <select id="select-other" style="display: none">
                <?php
                echo "<option selected >" . Yii::t('frontend', 'Choose a person') . "</option>";
                foreach ($arr as $item) {
                    echo "<option value=" . $item['id'] . ">" . $item['username'] . "</option>";
                }
                ?>

            </select>

        </div>


    </div>
    <div class="calendar-header" style="text-align: right;">
        <a  id="export" href="javascript:void(0)"><?= Yii::t('frontend', 'Export to Excel') ?></a>


    </div>
    <div class="note" style="margin-top: 20px; padding: 10px">
        <span class="glyphicon glyphicon-stop" style="color: orange">  <?= Yii::t('frontend', 'Inprogress') ?></span><br/>
        <span class="glyphicon glyphicon-stop"  style="color: green">  <?= Yii::t('frontend', 'Approved') ?></span><br/>
        <span class="glyphicon glyphicon-stop" style="color: red">  <?= Yii::t('frontend', 'Rejected') ?></span><br/>
    </div>
    <div id="fullcalendar">

<?= $this->render('calendar_view', array('events' => $events)); ?>
    </div>


</div>

