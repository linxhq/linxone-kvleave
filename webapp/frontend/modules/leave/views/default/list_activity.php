<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Menu;
use yii\bootstrap\Alert;
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use frontend\modules\leave\models\User;

$this->title = 'Trang chủ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div  id="wrapper-list-leave" >

    <style>
        #wrapper-list-leave {

            width: 100%;
    
            overflow: hidden; 
               min-height: 800px;
        } #mid {

            width: 100%;
            overflow: hidden; /* if you don't want #second to wrap below #first */
        } 
        .content{


        }

        @media  (min-width: 600px) {
            #wrapper-list-leave{
                height: 100%;
               
                min-height: 800px;
            }
            #left {
                float: left;

                width: 20%;
                float:left;  
                border: 1px solid red;
            }
            #right {
                float: left;

                width: 20%;

                overflow: hidden; 

            }
            #mid {
                float: left;
                width: 60%;
                overflow: hidden; /* if you don't want #second to wrap below #first */
            } 
        }
    </style>
    <div id="left" >


    </div>


    <div id="mid">

        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => [
                'tag' => 'div',
                'class' => 'list-wrapper',
                'id' => 'list-wrapper',
            ],
            'emptyText' => '',
            'itemOptions' => [
                'tag' => false,
            ],
            'summary' => '',
            'layout' => '{items}{pager}',
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'maxButtonCount' => 4,
                'options' => [
                    'class' => 'pagination col-xs-12'
                ]
            ],
            'itemView' => function ($model, $key, $index, $widget) {


                return $this->render('item_activity', ['model' => $model]);
            }
        ]);
        ?>

    </div>
    <div id="right" >


    </div>





</div>
