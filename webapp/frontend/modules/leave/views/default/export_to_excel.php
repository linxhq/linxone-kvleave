<?php

use moonland\phpexcel\Excel;
use frontend\modules\leave\models\Stream;


\moonland\phpexcel\Excel::export([
    'fileName' => "statistic_leave",
    'models' => $models,
    'columns' => [ 'title', 'start_time', 'end_time', 'note',], //without header working, because the header will be get label from attribute label. 
    'headers' => [ 'title' => 'Tiêu đề', 'note' => 'Ghi chú', 'start_time' => 'Bắt đầu', 'end_time' => 'Kết thúc'],
]);
?>