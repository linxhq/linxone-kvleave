<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
if ($model->attributes['isView'] == 1) {
    $color = "#FFF";
} else
    $color = "#edf2fa";
?>

<a class="item-notify" style="background-color:<?= $color ?> " href="view?id=<?= $model->attributes['stream_id'] ?>&id_notice=<?=$model->attributes['id'] ?>">
    <div style="display: inline-block">
        <?php
        if ($model->attributes['type'] == 'approved') {
            echo " <i class='glyphicon glyphicon-ok'></i>";
        } else {
            echo " <i class='glyphicon glyphicon-envelope'></i>";
        }
        ?>

        <b><span style="color:#434343;">  <?= $model->attributes['title'] ?> </span></b><br/>
    </div>
    <br/>

    <span style="font-size: 16px;color:#5d5d5d"> <?= $model->attributes['body'] ?> </span>



</a>
<!--<script>
    $('.item-notify').click(function() {
        $.ajax({
        url: 'requests/default/view',
        type: "POST",
        data: { limit: limit },
        success: function (result) {
            console.log(result);
        notice_container.html(result);
        },
    });
});

</script>-->

