<?php

use yii2fullcalendar\yii2fullcalendar;
use yii\web\JsExpression;


echo yii2fullcalendar::widget(array(
    'events' => $events,
    'header' => array(
        'center' => 'title',
        'left' => 'prev,next today',
        'right' => 'month,agendaWeek,agendaDay,listMonth'
    ),
    'options' => [
        'id' => 'calendar',
        'style' => [
            'width' => '100%',
        ],
    ],
    'eventClick' => "function(calEvent, jsEvent, view) {   
          //   location.href = 'view?id='+calEvent['id'];
              
       }",
));
?>