<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
?>
    
        <?=
        ListView::widget([
            'dataProvider' => $dataProvider,
            'options' => [
                'data-pjax' => true,
                'tag' => 'div',
                'class' => 'list-wrapper-manager',
                'id' => 'list-manager',
              
            ],
            'emptyText' => '',
            'itemOptions' => [
                'tag' => false,
            ],
            'summary' => '',
            'layout' => '{items}{pager}',
            'pager' => [
                'firstPageLabel' => 'First',
                'lastPageLabel' => 'Last',
                'maxButtonCount' => 4,
                'options' => [
                    'class' => 'pagination col-xs-12'
                ]
            ],
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('item_manage', ['model' => $model]);
            }
        ]);
        ?>