


<div class="tabs-container">
    <div class="tab-header sticky" id="tab">

        <button class="tablinks active" name="tab_progress"><?= Yii::t('frontend', 'In progress') ?></button>
        <button class="tablinks" name="tab_approved"><?= Yii::t('frontend', 'Approved') ?></button>
        <button class="tablinks" name="tab_rejected"><?= Yii::t('frontend', 'Rejected') ?></button>

    </div>

    <div class="tab-content" id="tab-content" >
        <?= $this->render('listview_manager', ['dataProvider' => $dataProvider]); ?>
    </div>


</div>