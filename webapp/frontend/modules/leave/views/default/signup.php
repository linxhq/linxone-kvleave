
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="login-container">
    <div class="card card-container">
        <h3><?= Yii::t('frontend', 'Sign up') ?></h3>
        <p id="profile-name" class="profile-name-card"></p>
        <div class="site-login">


            <?php
            $form = ActiveForm::begin(['id' => 'login-form',
                        'options' => [
                            'style' => 'width:100%'
                        ],
            ]);
            ?>
            <?=
            $form->field($model, 'username')->textInput(['maxlength' => 50])->label(Yii::t('frontend', 'Username'))
            ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => 100]) ?>

            <?= $form->field($model, 'password')->passwordInput(['maxlength' => 30])->label(Yii::t('frontend', 'Password')) ?>
            <?= $form->field($model, 'rePassword')->passwordInput(['maxlength' => 30])->label(Yii::t('frontend', 'Re-Password')) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('frontend', 'Sign up'), ['class' => 'btn btn-lg btn-primary btn-block btn-signin']) ?>
            </div>

            <?php ActiveForm::end(); ?>


        </div>

    </div><!-- /card-container -->
</div><!-- /container -->



