<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

$type = $model['leave_type'];
$type_string;

$status;
$color = "#000";
$approved = $model['approved'];
$approve_status = $model['approved_status'];

if ($approved == 1 && $approve_status == 0) {
    $status = Yii::t('frontend', 'Approved');
    $color = "$33cc33";
} else if ($approved == 0 && $approve_status == 1) {
    $status = Yii::t('frontend', 'Rejected');
    $color = "#ff3300";
} else if ($approved == 0 && $approve_status == 0) {
    $status = Yii::t('frontend', 'In Progress');
    $color = "#ff9900";
}

switch ($type) {
    case 1:
        $type_string = Yii::t('frontend', 'Sick leave');
        break;
    case 2:
        $type_string = Yii::t('frontend', 'Unpaid leave');

        break;
    case 3:
        $type_string = Yii::t('frontend', 'Be on leave');
        break;

    case 4:
        $type_string = Yii::t('frontend', 'Maternity leave');
        break;
}

$time_stamp = new DateTime($model['time_stamp']);
$dateStart = new DateTime($model['start_time']);
$dateEnd = new DateTime($model['end_time']);
$start_time = $dateStart->format('d-m-Y h:i a');
$end_time = $dateEnd->format('d-m-Y h:i a');
$time = $time_stamp->format('d-m-Y h:i a');
?>
<style>
    #details-table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #details-table td, #details-table th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #details-table tr:nth-child(even){background-color: #f2f2f2;}

    #details-table tr:hover {background-color: #ddd;}

    #details-table th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }
</style>
<div class="view-details">

    <div>
        <h3>
            <b><?= $type_string ?></b>
        </h3>
    </div>
    <table style="width:100%" id="details-table">

        <tr>

            <td><?= Yii::t('frontend', 'Start time') ?></td> 
            <td><?= $start_time ?></td>
        </tr>

        <tr>
            <td><?= Yii::t('frontend', 'End time') ?></td>
            <td><?= $end_time ?></td>

        </tr>
        <tr>
            <td><?= Yii::t('frontend', 'Notes') ?></td>
            <td><?= $model['note'] ?></td>

        </tr>

        <tr>
            <td><?= Yii::t('frontend', 'Status') ?></td>
            <td><b><font color="<?= $color ?>"><?= $status ?></font></b></td>

        </tr>
    </table>
    <?php
    if ($approved == 0 && $approve_status == 0 && $model['approver_id'] == Yii::$app->user->id)
    {
        echo "<div class='pull-right group-accept' style='display: inline-block; margin-top: 50px;'>";
        echo Html::a(Yii::t('frontend', 'Accept'), 'approve?id='.$model['id'].'&action=accept', ['class' => 'btn btn-success','style'=>[
            'margin-right'=>'6px',
        ]]) ;
        echo Html::a(Yii::t('frontend', 'Reject'), 'approve?id='.$model['id'].'&action=reject', ['class' => 'btn btn-default']);
    echo "</div>";
    }
    ?>
    

</div>
