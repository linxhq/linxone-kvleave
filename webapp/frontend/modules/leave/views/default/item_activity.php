<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\common\firebase\FirebaseHelper;

$type = $model['leave_type'];
$type_string;
$status;
$color = "#000";
$approved = $model['approved'];
$approve_status = $model['approved_status'];

if ($approved == 1 && $approve_status == 0) {
    $status = "Approved";
    $color = "$33cc33";
} else if ($approved == 0 && $approve_status == 1) {
    $status = "Reject";
    $color = "#ff3300";
} else if ($approved == 0 && $approve_status == 0) {
    $status = "In Progress";
    $color = "#ff9900";
}
switch ($type) {
    case 1:
        $type_string = Yii::t('frontend', 'Sick leave');
        break;
    case 2:
        $type_string = Yii::t('frontend', 'Unpaid leave');

        break;
    case 3:
        $type_string = Yii::t('frontend', 'Be on leave');
        break;

    case 4:
        $type_string = Yii::t('frontend', 'Maternity leave');
        break;
}
$time_stamp = new DateTime($model['time_stamp']);
$dateStart = new DateTime($model['start_time']);
$dateEnd = new DateTime($model['end_time']);
$start_time = $dateStart->format('d-m-Y h:i a');
$end_time = $dateEnd->format('d-m-Y h:i a');
$time = $time_stamp->format('d-m-Y h:i a');
?>


<div class="item-activity">
    <div class="status pull-right"> <b><font style="white-space: nowrap " color="<?= $color ?>"><?= $status ?></font></b></div>
    <div class="avatar" style="padding: 10px;">
        <img style="float: left" src="<?= Yii::$app->request->baseUrl . '/assets/activity/img/avatar.png' ?>"/>
        <div style="float: left">
            <b><?= Html::a($model['username'], '#') ?></b> <?= Yii::t('frontend', 'send to') ?> <b><?= Html::a($model['approver_name'], '#') ?></b>
            </br>
            <p> <?= $time ?></p>

        </div>




    </div>
    <div class="title" style="padding-left:  16px;clear: both; margin: 0 auto; ">
        <b><?= Html::a($type_string, "view?id=" . $model['id']) ?> </b>
    </div>

    <div class="time" style="padding-left:  16px;margin-top: 10px">
        <?= Yii::t('frontend', 'Start from:') ?>  <b><?= $start_time ?></b> <?= Yii::t('frontend', 'to') ?> <b><?= $end_time ?></b>
    </div>



</div>


