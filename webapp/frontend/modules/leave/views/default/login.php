<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title =Yii::t('frontend','Sign up');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="login-container" style="">
    <div class="card card-container">
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
        <p id="profile-name" class="profile-name-card"></p>
        <div class="site-login">


            <?php
            $form = ActiveForm::begin(['id' => 'login-form',
                        'options' => [
                            'style'=>'width:100%'
                        ],
            ]);
            ?>
            <?= $form->field($model, 'email')->textInput(['autofocus' => true, 'maxLength' => 100]) ?>

            <?= $form->field($model, 'password')->passwordInput(['maxLength' => 30])->label(Yii::t('frontend','Password')) ?>

<?= $form->field($model, 'rememberMe')->checkbox()->label(Yii::t('frontend','Remember password')) ?>


            <div class="form-group">
<?= Html::submitButton(Yii::t('frontend','Sign in'), ['class' => 'btn btn-lg btn-primary btn-block btn-signin', 'name' => 'login-button']) ?>
            </div>

<?php ActiveForm::end(); ?>

 <a href="#" class="forgot-password">
          <?= Yii::t('frontend','Forgot the password?') ?>
        </a>
        </div>
      
    </div><!-- /card-container -->
</div><!-- /container -->

