<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use \yii\widgets\ListView;
echo ListView::widget([
                'dataProvider' => $dataProvider,
                'options' => [
                    'data-pjax' => true,
                    'tag' => 'div',
                    'class' => 'list-wrapper-notify',
                    'id' => 'list-notify',
                ],
                'emptyText' => Yii::t('frontend', 'No notice'),
                'itemOptions' => [
                    'tag' => false,
                ],

                'layout' => '{items}',
              
                'itemView' => function ($model, $key, $index, $widget) {
                    return $this->render('list_item_notify', ['model' => $model]);
                }
            ]);