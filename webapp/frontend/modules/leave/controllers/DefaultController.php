<?php

namespace app\modules\leave\controllers;

use Yii;
use frontend\modules\leave\models\User;
use frontend\modules\leave\models\Stream;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\modules\leave\common\models\LoginForm;
use frontend\modules\leave\common\models\SignupForm;
use frontend\modules\leave\common\models\RequestForm;
use frontend\modules\leave\common\date\DateTimeFormatter;
use frontend\modules\leave\common\firebase\FirebaseHelper;
use frontend\modules\leave\models\Notify;
use frontend\modules\leave\components\DataLoad;


/**
 * Default controller for the `leave` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
 public function actionIndex() {

        if (Yii::$app->request->post() && \Yii::$app->request->isAjax) {
            $params = Yii::$app->request->post();
            $limit = $params['limit'];

            $dataLoad = new DataLoad;
            $dataProvider = $dataLoad->loadDataNotify($limit);
            return $this->renderPartial('listview_notice', ['dataProvider' => $dataProvider]);
        }
//        if (\Yii::$app->user->isGuest) {
//            return $this->render('index');
//        }
        $model = new Stream();
        $id = Yii::$app->user->id;
        $result = [];
        if ($id) {
            $arr = $model->find()->where('user_id = ' . $id)->orderBy(['id' => SORT_DESC])->all();

            foreach ($arr as $a) {
                $a->getUser();
                $a->getApprover();
                $res = $a->getAttributes();

                $res['username'] = $a->user->getAttribute('username');
                $res['approver_name'] = $a->approver->getAttribute('username');
                $result[] = $res;
            }
        }
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $result,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        if (count($result) > 0) {

            return $this->render('list_activity', array('dataProvider' => $dataProvider));
        } else {
            return $this->render('empty_listview');
        }
    }

    public function actionCreate() {
        $model = new RequestForm();
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {

            
            $params = Yii::$app->request->post();
            if (isset($params['sendto']))
                $sendto = $params['sendto'][0]['id'];
            $arr = [];
            
            $model->approverId = $sendto;
            if (isset($params['cc'])) {
                $model->cc = json_encode($params['cc']);
                $arr = $params['cc'];
            }
            
            $model->type = $params['RequestForm'][1]['value'];
            $model->startTime = $params['RequestForm'][2]['value'];
            $model->endTime = $params['RequestForm'][3]['value'];
            $model->note = $params['RequestForm'][4]['value'];
    

            if ($model->add($model) && $model->validateDate()) {
                $stream = new Stream();

                $client_keys = $stream->getTokensUser($model->approverId);
                $title = $stream->getUsername(Yii::$app->user->id) . ' request leave';
                $body = $model->note;
                $type = 'send_request';
                $data = array(
                    'type' => $type,
                    'title' => $title,
                    'body' => $body,
                );

                if ($client_keys != null)
                {
                   
                    foreach ($client_keys as $token) {
                        
                        if ($token->code != null)
                            $this->sendNotification($token->code, $data, $title, $body);
                    }
                }

                $this->notifyWithId($model->approverId, $title, $body, $type, $model->id);
                if (count($arr) > 0) {
                    foreach ($arr as $item) {

                        $cc_tokens = $stream->getTokensUser($item['id']);
                        if ($cc_tokens != null)
                            foreach ($cc_tokens as $token) {
                                if ($token->code != null)
                                    $this->sendNotification($token->code, $data, $model->getUsername(Yii::$app->user->id) . ' send a request to ' . $model->getUsername($model->approverId), $body);
                            }
                        $this->notifyWithId($item['id'], $stream->getUsername(Yii::$app->user->id) . ' send a request to ' . $stream->getUsername($model->approverId), $body, 'cc', $model->id);
                    };
                }
               
                return true;
            } else {
  
                return false;
            }
        } else
            return $this->render('requestForm', array('model' => $model));
    }

    function actionView($id) {

        if (isset(Yii::$app->request->get()['id_notice'])) {
            $id_notice = Yii::$app->request->get()['id_notice'];
            $noti = Notify::find()->where(['id' => $id_notice])->one();
            if ($noti->isView == 0) {
                $noti->isView = 1;
                $noti->save();
            }
        }
        $model;

        $stream = new Stream;
        $res = $stream->findById($id);
        $res->getUser();
        $res->getApprover();
        $model = $res->getAttributes();
        $model['username'] = $res->user->getAttribute('username');
        $model['approver_name'] = $res->approver->getAttribute('username');

        return $this->render('view', array(
                    'model' => $model
        ));
    }

    public function actionApprove($id, $action) {

        $stream = new Stream();
        $approved = 0;
        $approved_status = 0;
        $model = $stream->findById($id);
        if ($model != null) {
            if ($action == 'accept') {
                $approved = 1;
                $approved_status = 0;
            } else if ($action == 'reject') {
                $approved = 0;
                $approved_status = 1;
            }

            $model->approved = $approved;
            $model->approved_status = $approved_status;

            if ($model->save()) {
                $client_keys = $stream->getTokensUser($stream->getUserIdRequest($id));
                
                
                $title = $model->getUsername($model->approver_id) . ' approved your leave request';
                $body = $model->attributes['note'];
                $type = 'approved';
                $data = array(
                    'type' => $type,
                    'title' => $title,
                    'body' => $body,
                    'stream_id' => $id
                );

                    if ($client_keys != null)
                    foreach ($client_keys as $token) {
                        if ($token->code != null)
                            $this->sendNotification($token->code, $data, $title, $body);
                    }
                
                
                $this->notifyWithId($stream->getUserIdRequest($id), $title, $body, $type, $id);
                return $this->render('view', array('model' => $model));
            }
        }
    }

    public function notifyWithId($id, $title, $body, $type, $streamId) {
        $noti = new Notify();
        $noti->add($id, $title, $body, $type, $streamId);
    }

    public function sendNotification($token, $data, $message, $body) {
        if ($token != null)
            FirebaseHelper::sendPushNotification($token, $data, $message, $body);
    }
    
    
    
     public function actionManager() {
        $model = new Stream();
        $id = Yii::$app->user->id;

        $result = [];

        if (Yii::$app->request->post()) {
            $params = Yii::$app->request->post();

            $approved = 0;
            $approved_status = 0;
            switch ($params['type']) {
                case 'inprogress':
                    $approved = 0;
                    $approved_status = 0;
                    break;
                case 'approved':
                    $approved = 1;
                    $approved_status = 0;
                    break;
                case 'rejected':
                    $approved = 0;
                    $approved_status = 1;
                    break;
            }

            if ($id) {
                $arr = $model->find()->where(
                                        [
                                            'approved' => $approved,
                                            'approved_status' => $approved_status,
                                        ]
                                )
                                ->orderBy(['id' => SORT_DESC])->groupBy('id')->all();


                foreach ($arr as $item) {
                    $item->getUser();
                    $item->getApprover();
                    $res = $item->getAttributes();
                    $arr_cc = json_decode($res['cc'], true);

                    $res['username'] = $item->user->getAttribute('username');
                    $res['approver_name'] = $item->approver->getAttribute('username');

                    if ($this->isCc($arr_cc, $id) || $res['approver_id'] == $id) {
                        $result[] = $res;
                    }
                }
            }

            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            if (count($result) > 0)
                return $this->renderPartial('listview_manager', array('dataProvider' => $dataProvider));
            else {
                return $this->renderPartial('empty_manager');
            }
        } else {
            $dataProvider = new \yii\data\ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            return $this->render('manager', array('dataProvider' => $dataProvider));
        }
    }

    public function isCc($arr, $id) {

        if ($arr == null || count($arr) == 0)
            return FALSE;
        foreach ($arr as $item) {
            if ($item['id'] == $id) {
                return true;
            }
        }
        return false;
    }
    
       public function actionLogin() {


        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }
   public function actionSignup() {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            $params = Yii::$app->request->post();

            $model->username = $params['SignupForm']['username'];
            $model->password = $params['SignupForm']['password'];
            $model->email = $params['SignupForm']['email'];
            if ($model->signup()) {
                Yii::$app->user->login($model->getUser());
                return $this->goHome();
            } else {
                return $this->render('signup', [
                            'model' => $model,
                ]);
            }
        } else {
            return $this->render('signup', [
                        'model' => $model,
            ]);
        }
    }
    
    
     private function getNameRequest($id) {
        $user = \frontend\modules\leave\models\User::find()->where(['id' => $id])->one();
        return ($user->attributes['username']);
    }

    private function getTypeLeave($type) {
        switch ($type) {
            case 1:
                return Yii::t('frontend', 'Sick leave');
                break;
            case 2:
                return Yii::t('frontend', 'Unpaid leave');

                break;
            case 3:
                return Yii::t('frontend', 'Be on leave');
                break;

            case 4:
                return Yii::t('frontend', 'Maternity leave');
                break;
            default :
                return "Unknown";
        }
    }

    public function actionStatistic() {
        $model = new Stream;

        if (\Yii::$app->request->post() && \Yii::$app->request->isAjax) {
            $params = \Yii::$app->request->post();
            if ($params['type_select'] == "other") {

                $id = $params['id'];
                $arr = $model->find()->where([
                            'user_id' => $id,
                        ])->asArray()->all();
            } else if ($params['type_select'] == "all") {


                $arr = $model->find()->asArray()->all();
            } else {

                $arr = $model->find()->where([
                            'user_id' => \Yii::$app->user->id,
                        ])->asArray()->all();
            }
        } else {

            $arr = $model->find()->where([
                        'user_id' => \Yii::$app->user->id,
                    ])->asArray()->all();
        }


        $events = array();

        foreach ($arr as $item) {
            $event = new \yii2fullcalendar\models\Event();

            $event->backgroundColor = $this->getStatus($item['approved'], $item['approved_status']);
            $event->textColor = "#FFF";

            $event->id = $item['id'];
            $event->title = $this->getNameRequest($item['user_id']);
            $event->start = date('Y-m-d\TH:i:s\Z', strtotime($item['start_time']));
            $event->end = date('Y-m-d\TH:i:s\Z', strtotime($item['end_time']));
            $event->nonstandard = [
                'userId' => \Yii::$app->user->id,
                'timeStamp' => $item['time_stamp'],
                'note' => $item['note'],
                'cc' => $item['cc'],
                'approved' => $item['approved'],
                'approvedStatus' => $item['approved_status'],
                'approverId' => $item['approver_id'],
            ];
            $events[] = $event;
        }
        if (\Yii::$app->request->isAjax) {

            return $this->renderAjax('calendar_view', array('events' => $events));
        }

        return $this->render('statistic', array('events' => $events));
    }

    private function getStatus($approved, $approve_status) {

        $color = "ff9900";
        if ($approved == 1 && $approve_status == 0) {
            $status = Yii::t('frontend', 'Approved');
            $color = "#33cc33";
        } else if ($approved == 0 && $approve_status == 1) {
            $status = Yii::t('frontend', 'Rejected');
            $color = "#ff3300";
        } else if ($approved == 0 && $approve_status == 0) {
            $status = Yii::t('frontend', 'In Progress');
            $color = "#ff9900";
        }
        return $color;
    }

    public function actionExport() {
        $model = new Stream;
        $params = \Yii::$app->request->get();
        if ($params['type'] == "other") {

            $id = $params['id'];
            $arr = $model->find()->where([
                        'user_id' => $id,
                    ])->asArray()->all();
        } else if ($params['type'] == "all") {


            $arr = $model->find()->asArray()->all();
        } else {

            $arr = $model->find()->where([
                        'user_id' => \Yii::$app->user->id,
                    ])->asArray()->all();
        }

        return $this->render('export_to_excel', ['models' => $arr]);
    }
    
    
      public function actionLogout() {
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->logout()) {

                 $this->redirect("login");
            }
        }
    }
    
    
     public function actionChangelanguage() {

        if (isset($_REQUEST['lang']) && $_REQUEST['lang'] != null) {
            $lang = $_REQUEST['lang'];
            Yii::$app->language = $lang;
            $cookie = new \yii\web\Cookie([
                'name' => 'lang',
                'value' => $_REQUEST['lang'],
            ]);
            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
    }
}
