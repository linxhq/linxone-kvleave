<?php

namespace app\modules\leave\controllers;


use Yii;
use yii\web\Controller;
use frontend\modules\leave\models\Stream;
use frontend;

use yii\db\Expression;
use frontend\modules\leave\common\json\JsonResponseFormater;
use frontend\modules\leave\common\firebase\FirebaseHelper;
use frontend\modules\leave\models\Notify;
use yii\web\Response;
use frontend\modules\leave\models\User;








class ApiController extends \yii\rest\ActiveController
{
    
            public $modelClass = Stream::class;
     public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }
    
    public function actionIndex(){
        return "?âs";
}

    public function actionGetListNotify()
    {
        $params = \yii::$app->request->post();
        $noti = new Notify();
        $id =$params['id'];
        $arr = $noti->listById($id);
        $res = [];
        foreach ($arr as $item)
        {
          $res[] = $item->attributes;  
        }
          if ($res != null) {
            echo JsonResponseFormater::formatArr(true, $res, 'Success!');
        } else {
            echo JsonResponseFormater::formatArr(FALSE, null, 'Empty');
        }
      
        
    }
    
    


    public function notifyWithId($id, $title, $body, $type, $streamId) {
        $noti = new Notify();

        $noti->add($id, $title, $body, $type, $streamId);
    }

    public function actionAddLeave() {

        $model = new Stream();
        $params = \yii::$app->request->post();

        $model->attributes = $params;
        $model->time_stamp = new Expression('NOW()');
        $id_approver = $params['approver_id'];
        $id_user = $params['user_id'];
        $note = $params['note'];
        $cc = $params['cc'];
     
        
        $arr = json_decode($cc,true);
        
        if ($model->save()) {

            $client_key = $model->getTokenUser($id_approver);
            $title = $model->getUsername($id_user) . ' request leave';
            $body = $note;
            $type = 'send_request';
            $data = array(
                'type' => $type,
                'title' => $title,
                'body' => $body,
            );

            if ($client_key != null)
                FirebaseHelper::sendPushNotification($client_key, $data, $title, $body);
            $this->notifyWithId($id_approver, $title, $body, $type, $model->id);
            foreach ($arr as $item) {
                $client_key_cc = $model->getTokenUser($item['id']);
                if ($client_key_cc != null)
                    FirebaseHelper::sendPushNotification($client_key_cc, $data, $model->getUsername($id_user) . ' send a request to ' . $model->getUsername($id_approver), $body);


                $this->notifyWithId($item, $model->getUsername($id_user) . ' send a request to ' . $model->getUsername($id_approver), $body, 'cc', $model->id);
            };




            echo JsonResponseFormater::format(true, $model, 'Added');
        } else {
            echo JsonResponseFormater::format(FALSE, null, 'failed');
        }
    }

    public function actionGetStreams() {
        $params = \yii::$app->request->post();
        $id = $params['id'];
        $stream = new Stream();
        $arr = $stream->find()->where('user_id = ' . $id)->orderBy(['id' => SORT_DESC])->all();
        $result = [];
        foreach ($arr as $a) {
            $a->getUser();
            $a->getApprover();
            $res = $a->getAttributes();
            $res['username'] = $a->user->getAttribute('username');
            $res['approver_name'] = $a->approver->getAttribute('username');
            $result[] = $res;
        }
        if ($result != null) {
            echo JsonResponseFormater::formatArr(true, $result, 'Success!');
        } else {
            echo JsonResponseFormater::formatArr(FALSE, null, 'Empty');
        }
    }

    public function actionApprove() {
        $stream = new Stream();
        $params = \yii::$app->request->post();
        $id = $params['id'];
        $model = $stream->findById($id);
        if ($model != null) {
            $model->approved = $params['approved']; //0,1 
            $model->approved_status = $params['approved_status']; //0,1 reject
            $model->approver_id = $params['approver_id'];

            if ($model->save()) {

                $client_key = $stream->getTokenUser($stream->getUserIdRequest($id));
                $title = $model->getUsername($model->approver_id) . ' approved your leave request';
                $body = $model->attributes['note'];

                $type = 'approved';

                $data = array(
                    'type' => $type,
                    'title' => $title,
                    'body' => $body,
                    'stream_id' => $id
                );

                if ($client_key != null)
                    FirebaseHelper::sendPushNotification($client_key, $data, $title, $body);
                $this->notifyWithId($stream->getUserIdRequest($id), $title, $body, $type, $id);
                echo JsonResponseFormater::format(true, $model, 'Approved successfully!');
            } else {
                echo JsonResponseFormater::format(FALSE, null, 'Approved failed!');
            }
        }
    }

    public function actionGetLeaves() {

        $stream = new Stream();
        $params = \yii::$app->request->post();
        $id = $params['id'];
        $model = $stream->findById($id);
        if ($model != null) {

            echo JsonResponseFormater::format(true, $model, 'Success!');
        } else {
            echo JsonResponseFormater::format(FALSE, null, 'Not found!');
        }
    }

    public function actionGetInprogress() {
        $params = \yii::$app->request->post();
        $id = $params['id'];

        $stream = new Stream();

        $arr = $stream->find()->where(
                                [
                                    'approved' => 0,
                                    'approved_status' => 0,
                                ]
                        )
                        ->orderBy(['id' => SORT_DESC])->groupBy('id')->all();
        $result = [];
        foreach ($arr as $item) {
            $item->getUser();
            $item->getApprover();
            $res = $item->getAttributes();
            $arr_cc = json_decode($res['cc'], true);

            $res['username'] = $item->user->getAttribute('username');
            $res['approver_name'] = $item->approver->getAttribute('username');

            if ($this->isCc($arr_cc, $id) || $res['approver_id'] == $id) {
                $result[] = $res;
            }
        }

        if ($result != null) {
            echo JsonResponseFormater::formatArr(true, $result, 'Success!');
        } else {
            echo JsonResponseFormater::formatArr(FALSE, null, 'Not found!');
        }
    }

    public function isCc($arr, $id) {
        if (count($arr) == 0)
            return FALSE;
        foreach ($arr as $item) {
            if ($item['id'] == $id) {
                return true;
            }
        }
        return false;
    }

    public function actionGetReject() {
        $params = \yii::$app->request->post();
        $id = $params['id'];

        $stream = new Stream();


        $s = $arr = $stream->find()->where(
                                [
                                    'approved' => 0,
                                    'approved_status' => 1,
                                ]
                        )
                        ->orderBy(['id' => SORT_DESC])->groupBy('id')->all();
        $result = [];

        foreach ($arr as $item) {

            $item->getUser();
            $item->getApprover();
            $res = $item->getAttributes();
            $arr_cc = json_decode($res['cc'], true);
            $res['username'] = $item->user->getAttribute('username');
            $res['approver_name'] = $item->approver->getAttribute('username');
            if ($this->isCc($arr_cc, $id) || $res['approver_id'] == $id) {
                $result[] = $res;
            }
        }

        if ($result != null) {

            echo JsonResponseFormater::formatArr(true, $result, 'Success!');
        } else {
            echo JsonResponseFormater::formatArr(FALSE, null, 'Not found!');
        }
    }

    public function actionGetApproved() {
        $params = \yii::$app->request->post();
        $id = $params['id'];
        $stream = new Stream();
        $s = $arr = $stream->find()->where(
                                [
                                    'approved' => 1,
                                    'approved_status' => 0,
                                ]
                        )
                        ->orderBy(['id' => SORT_DESC])->groupBy('id')->all();
        $result = [];
        foreach ($arr as $item) {
            $item->getUser();
            $item->getApprover();
            $res = $item->getAttributes();
            $arr_cc = json_decode($res['cc'], true);
            $res['username'] = $item->user->getAttribute('username');
            $res['approver_name'] = $item->approver->getAttribute('username');
            if ($this->isCc($arr_cc, $id) || $res['approver_id'] == $id) {
                $result[] = $res;
            }
        }
        if ($result != null) {

            echo JsonResponseFormater::formatArr(true, $result, 'Success!');
        } else {
            echo JsonResponseFormater::formatArr(FALSE, null, 'Not found!');
        }
    }

    public function actionGetApprover() {
        $user = new User();
        
        $params = \yii::$app->request->post();
        $id = $params['id'];
        $arr = $user->getApprovers($id);
        
        if ($arr != null) {
            echo JsonResponseFormater::formatArr(true, $arr, 'Success!');
        } else {
            echo JsonResponseFormater::formatArr(FALSE, null, 'failed');
        }
    }

    public function actionLogout() {
        $user = new User();
        $params = \yii::$app->request->post();
        $email = $params['email'];
        // token request
        $token = $params['token'];
                // $password = $params['password'];
        $result = $user->logOut($email,$token);
        if ($result) {
            echo JsonResponseFormater::format(true, $result, 'Logout successfully!');
        } else {
            echo JsonResponseFormater::format(FALSE, null, 'Do not logout, please try again! ');
        }
    }

    public function actionLogin() {
    
        $user = new User();
        $params = \yii::$app->request->post();
        $email = $params['email'];
        $password = $params['password'];
        $token = $params['token'];

        
        $result = $user->accountInvalid($email, $user->hashPassword($password), $token);
        if ($result) {

            echo JsonResponseFormater::format(true, $result, 'Login successfully!');
        } else {
            echo JsonResponseFormater::format(FALSE, null, 'Email or password incorrect');
        }
    }

    public function actionGetUser() {
        $user = new User();
        $params = \yii::$app->request->post();

        $u = $user->findById($params['id']);
        if ($u) {

            echo JsonResponseFormater::format(true, $u, 'success!');
        } else {
            echo JsonResponseFormater::format(FALSE, null, 'not found!');
        }
    }

    public function actionRegister() {
        $model = new User();
        $model->attributes = \yii::$app->request->post();
        $model->password_hash = $model->validatePassword($model->password_hash);

        if ($model->invalidEmail($model->email)) {
            echo JsonResponseFormater::format(FALSE, $model, 'Email already exists');
        } else {



            if ($model->save()) {
                echo \JsonResponseFormater::format(true, $model, 'Register is successfuly');
            } else {
                echo \JsonResponseFormater::format(FALSE, $model, 'Fail');
            }
        }
    }

    
}
