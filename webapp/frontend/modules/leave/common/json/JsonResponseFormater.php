<?php

namespace frontend\modules\leave\common\json;
class JsonResponseFormater {

    public static function format($status, $model, $message) {
        if ($status)
            return json_encode(array('status' => $status, 'message' => $message, 'data' => array_filter($model->attributes)), JSON_PRETTY_PRINT);
        else
            return json_encode(array('status' => $status, 'message' => $message), JSON_PRETTY_PRINT);
    }

   public static function formatArr($status, $arr, $message) {
        if ($status)
            return json_encode(array('status' => $status, 'message' => $message, 'data' => $arr));
        else
            return json_encode(array('status' => $status, 'message' => $message), JSON_PRETTY_PRINT);
    }

}
