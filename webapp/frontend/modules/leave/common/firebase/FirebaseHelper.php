<?php

namespace frontend\modules\leave\common\firebase;
class FirebaseHelper {

    public static function sendPushNotification($client_key, $data, $title, $body) {
        $url = 'https://android.googleapis.com/gcm/send';
        $server_key = 'AAAA1Cmh5x0:APA91bHYqMJHIZR4l9RB3CTETtN_GJkjRTWPzXdHITjH7zUL6VFo0mF1-jhlqaSePrloBcwX05buGzXkJ7ae8Wt7v3ddXphQfda3yVZ7HNaHLWm7A51Nm4SvyfD9WtCgX5Z5o0VvPbaM';

        $headers = array(
            'Authorization:key=' . $server_key,
            'Content-Type:application/json'
        );
        $fields = array(
            'to' => $client_key,
            'priority' => 'high',
            'notification' => array(
                'title' => $title,
                'body' => $body,
                'sound' => 'default',
                'android_channel_id'=>'fcm_default_channel'
            ),
            'data' => $data
        );

        $payload = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Oops! FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }

}
