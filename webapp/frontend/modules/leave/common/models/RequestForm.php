<?php

namespace frontend\modules\leave\common\models;

use Yii;
use yii\base\Model;
use frontend\modules\leave\models\Stream;
use frontend\modules\leave\common\date\DateTimeFormatter;

/**
 * Login form
 */
class RequestForm extends Model {
    public $id;
    public $isSuccess;
    public $type;
    public $note;
    public $startTime;
    public $endTime;
    public $approverId;
    public $cc;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['type', 'startTime', 'endTime', 'endTime'], 'required'],
        ];
    }

    public function attributeLabels() {
        return array(
            'type' => 'Loại nghỉ phép',
            'startTime' => 'Thời gian bắt đầu',
            'endTime' => 'Thời gian kết thúc',
            'approverId' => 'Gửi tới',
            'note' => 'Ghi chú',
            'cc' => 'CC'
        );
    }

    public function add($model) {
        $stream = new Stream();
             $modelSaved = $stream->addLeave($model);
     
        if ($modelSaved) {
            

             $this->id = $modelSaved->attributes['id'];
            return true;
        }
        return FALSE;
    }

    public function validateDate() {
        if (!$this->hasErrors()) {

            $start_time = date('Y-m-d G:i:s', strtotime($this->startTime));
              $end_time = date('Y-m-d G:i:s', strtotime($this->endTime));

            if (DateTimeFormatter::isWeekend($start_time) || DateTimeFormatter::isWeekend($end_time) ) {
                return FALSE;
            } else
                return true;
        }
    }
 
    
    
  

}
