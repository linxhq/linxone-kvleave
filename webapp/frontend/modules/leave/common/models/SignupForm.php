<?php

namespace frontend\modules\leave\common\models;

use Yii;
use yii\base\Model;
use frontend\modules\leave\models\User;

/**
 * Login form
 */
class SignupForm extends Model
{


    public $username;
    public $email;
    public $password;
    public $rePassword ;




    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password','username','rePassword'], 'required']

         
        ];
    }
  public function attributeLabels() {
        return [
            'email' => 'Email',
            'password' => 'Password',
            'username' => 'Username',
            'rePassword' => 'Re-Password',
            'captcha'=>'Confirm'
        ];
    }
    public function validateEmail()
    {
        if (!$this->hasErrors()) {
            $user = User::findByEmail($this->email);
            if ($user) {
                $this->addError('email', 'Email aldreay exist');
                return false;
            }
        }
        
        
        return true;
        
        
        
    }
    public function signup()
    {
         if($this->validateEmail())
         {
            $user = new User();
            $user->username =$this->username;
            $user->email = $this->email;
            $user->password_hash= $user->hashPassword($this->password);
            if($user->save())
            {
                return true;
            }
         }
       return false;  
    }
    public function getUser()
    {
        return User::findByEmail($this->email);
    }
}
