<?php

namespace frontend\modules\leave\models;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use frontend\modules\leave\models\User;
use yii\db\Expression;

class Stream extends ActiveRecord {
    public $streamId;
    public $user;
    public $approver;

    const SCENARIO_CREATE = 'create';

    public function scenarios() {
        $scenarios = parent::scenarios();

        $scenarios['create'] = ['user_id', 'time_stamp',
            'start_time', 'end_time', 'leave_type', 'note', 'approved',
            'approved_status', 'approver_id', 'cc'
        ];
        return $scenarios;
    }

    public static function tableName() {
        return 'leave_content';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['user_id', 'time_stamp', 'end_time', 'start_time', 'leave_type'], 'required'],
            [['note', 'cc'], 'string'],
            [['user_id', 'approved_status', 'approver_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_id' => 'ID User',
            'time_stamp' => 'Time stamp',
            'start_time' => 'Start time',
            'end_time' => 'End time',
            'leave_type' => 'Leave Type',
            'note' => 'Notes',
            'approved' => 'Approved',
            'approved_status' => 'Status',
            'approver_id' => 'ID approved',
            'cc' => 'cc'
        ];
    }

    public function findById($id) {
        return $this->findOne($id);
    }

    public function listById($id) {


        $query = (new \yii\db\Query())
                ->select('user_id,'
                        . 'start_time,'
                        . 'end_time,'
                        . 'leave_type,'
                        . 'note,cc,approved,approved_status,approver_id,username,email,role_id')
                ->from('leave_content')
                ->where('user_id = ' . $id)
                ->rightJoin('user', 'leave_content.approver_id=user.id');


        $command = $query->createCommand();

        $rows = $command->queryAll();

        $user = $this->getUser();
        var_dump($this);
        die;
        if (!count($rows)) {
            return null;
        }
        return $rows;
    }

    public function getUser() {
        $this->user = $this->hasOne(User::className(), ['id' => 'user_id'])->one();
    }

    public function getTokensUser($id) {
        $token = new Token();

        $result = $token->getTokens($id);
        return $result;
    }

    public function getUserIdRequest($id) {
        $stream = new Stream();
        $res = $stream->findById($id);
        return $res->attributes['user_id'];
    }

    public function getUsername($id) {
        $user = new User();

        $result = $user->findById($id);

        return $result->attributes['username'];
    }

    public function getApprover() {
        $this->approver = $this->hasOne(User::className(), ['id' => 'approver_id'])->one();
    }

    public function addLeave($data) {

        $model = new Stream();
        $model->user_id = Yii::$app->user->id;
        $model->time_stamp = new Expression('NOW()');
        $model->start_time = date('Y-m-d G:i:s', strtotime($data->startTime));
        $model->end_time = date('Y-m-d G:i:s',strtotime($data->endTime));
        $model->leave_type = $data->type;
        $model->note = $data->note;
        $model->approver_id = $data->approverId;
        $model->cc = $data->cc;
        if($model->save())
        {       
            return $model;
            
        }
        return FALSE;
    }

}
