<?php
namespace frontend\modules\leave\models;

use Yii;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class PersonalProfiles extends \yii\db\ActiveRecord
{
    var $full_name = '';
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal_profiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['person_given_name'], 'required'],
            [['person_dob'], 'safe'],
            [['is_active', 'gitlab_id'], 'integer'],
            [['person_given_name'], 'string', 'max' => 100],
            [['person_family_name', 'person_mobile'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'person_given_name' => Yii::t('frontend', 'Person Given Name'),
            'person_family_name' => Yii::t('frontend', 'Person Family Name'),
            'person_mobile' => Yii::t('frontend', 'Person Mobile'),
            'person_dob' => Yii::t('frontend', 'Person Dob'),
            'is_active' => Yii::t('frontend', 'Active'),
            'gitlab_id' => Yii::t('frontend', 'GitLab ID'),
        ];
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->full_name = $this->getFullName();
    }

    public static function getActiveProfiles()
    {
        return PersonalProfiles::find()->where(['is_active' => 1])->orderBy('person_family_name ASC')->all();
    }

    public function getFullName()
    {
        return $this->person_family_name . ' ' . $this->person_given_name;
    }

    public static function getProfileByUserId($user_id)
    {
        return PersonalProfiles::find()->where(['user_id' => $user_id])->one();
    }
}