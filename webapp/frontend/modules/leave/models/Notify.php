<?php

namespace frontend\modules\leave\models;

use Yii;
/**
 * This is the model class for table "notify".
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $body
 */
use \yii\db\ActiveRecord;

class Notify extends ActiveRecord {

    const SCENARIO_CREATE = 'create';

    public function scenarios() {
        $scenarios = parent::scenarios();

        $scenarios['create'] = ['user_id', 'stream_id', 'title',
            'body', 'type', 'isView'
        ];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'notify';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'isView', 'stream_id'], 'integer'],
            [['title', 'body', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'body' => 'Body',
            'type' => 'Type',
            'stream_id' => 'Stream ID'
        ];
    }

    public function findById($id) {
        return $this->findOne($id);
    }

    public function listById($id) {
        $arr = $this->find()->where('user_id= ' . $id)->orderBy(['id' => SORT_DESC])->all();
        return $arr;
    }

    public function add($id, $title, $body, $type, $streamId) {
        $noti = new Notify();
        $noti->user_id = $id;
        $noti->stream_id = $streamId;
        $noti->title = $title;
        $noti->body = $body;
        $noti->type = $type;
        $noti->save();
    }
    
    public function getDataLimited($limit)
    {
        $arr = self::find()->where(['user_id' => Yii::$app->user->id])->limit($limit)->orderBy(['id' => SORT_DESC])->all();
        return $arr;
    }
    public function getCountNoticeUser()
    {
        return self::find()->where(['user_id' => Yii::$app->user->id])->count();
    }
    public function getNotificationCountUser()
    {
        $notifyModels = self::find()->where(['user_id'=> Yii::$app->user->id, 'isView'=>0])->asArray()->all();
        $count = count($notifyModels);
        return $count;
    }
}
