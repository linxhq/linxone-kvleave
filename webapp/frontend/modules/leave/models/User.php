<?php

namespace frontend\modules\leave\models;

use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface {

    public $rememberMe = true;

    public static function tableName() {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['username', 'email', 'password_hash'], 'required'],
            [['flags', 'confirmed_at', 'blocked_at', 'updated_at', 'created_at', 'last_login_at'], 'integer'],
            [['username', 'email', 'password_hash', 'unconfirmed_email', 'registration_ip'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 50],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password_hash' => 'Password',
            'auth_key' => 'Auth Key',
        ];
    }

    public function findById($id) {

        return $this->findOne($id);
    }

    public static function findByEmail($email) {

        return self::findOne(['email' => $email]);
    }

    public function logOut($email, $token) {
        $user = $this->find()
                ->where([
                    "email" => $email
                ])
                ->one();
        if (!$user)
            return null;
        else {

            $t = new Token;
            if ($t->tokenIsExist($token)) {
                $model = Token::findOne(['code'=>$token]);
                if($model!=null)
                    $model->delete();
            }
        }
        return $user;
    }

    public function accountInvalid($email, $password, $token) {


        $user = $this->find()
                ->where([
                    "email" => $email,
                    "password_hash" => $password
                ])
                ->one();
        if (!$user)
            return null;
        else {

            if ($token != null) {
                $t = new Token;
                if (!$t->tokenIsExist($token)) {
                    $t->code = $token;
                    $t->user_id = $user->id;
                    strtotime(date('Y-m-d H:i:s'));
                    $t->created_at = strtotime(date('Y-m-d H:i:s'));
                    $t->type = 0;
                    $t->save();
                }
            }
        }



        return $user;
    }

    public function validatePassword($password) {
        return $this->password_hash === md5($password);
    }

    public function hashPassword($password) {
        return md5($password);
    }

    public function invalidEmail($email) {
        $user = self::find()
                ->where([
                    "email" => $email
                ])
                ->one();
        if ($user) {
            return true;
        }
        return false;
    }

    public function getApprovers($id) {

        $arr = $this->find()->select('*')->andWhere(['<>', 'id', $id])->asArray()->all();

        if (count($arr) == 0) {
            return null;
        }
        return $arr;
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        return $this->auth_key == $authKey;
    }

    public static function getAllUsers() {
        return self::find()->all();
    }

    public static function findIdentity($id) {

        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new \yii\base\NotSupportedException();
    }

    public static function getNotificationCount() {
        if (!\Yii::$app->user->isGuest) {
            $noti = new Notify;
            return $noti->getNotificationCountUser();
        }
        return;
    }

}
