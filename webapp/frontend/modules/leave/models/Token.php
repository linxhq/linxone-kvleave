<?php

namespace frontend\modules\leave\models;

use yii\base\Model;
use yii\db\ActiveRecord;

class Token extends ActiveRecord {

    public static function tableName() {
        return 'token';
    }

    public static function primaryKey() {
        return ['code'];
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id'], 'required'],
            [['type', 'created_at'], 'integer'],
            [['code'], 'string', 'max' => 255],
        ];
    }

    public function getTokens($id) {
        $arr = self::find()->where(['user_id' => $id])->asArray()->all();
        if (count($arr) > 0)
            return $arr;
        return null;
    }

    public function tokenIsExist($token) {
        return self::find()
                        ->where(['code' => $token])
                        ->exists();
    }

}
