var x = document.getElementById("emailField");
var arrSend = [];
var arrCc = [];
var sendto = document.getElementById("sendto");
var cc = document.getElementById("cc");
var isfocus = true;
var a = [];
var isHide = true;
var complete = false;

$(".taginput-cc .tokenize").hide();

$("#click-cc").on("click", function () {
    if (isHide && $("#sendto :selected").length > 0)
    {
        $(".taginput-cc .tokenize").show();
        
        isHide = false;
        
      
    } else {

        $(".taginput-cc .tokenize").hide();

        isHide = true;
       
    }
})

$(".taginput-send input").focus(function ()
{
    if (a.length == 0)
    {
        a = sendto.innerHTML;
    }


    if (isfocus)
    {
        $(".taginput-send ul.tokens-container.form-control").css("border", "1px solid #000000");
        $("#send-to-label").css("color", "#000000");
        x.style.display = "none";
        isKey = false;
    }

});



$(".taginput-send input").keydown(function () {
   

    if ($("#sendto :selected").length == 0)
    {
        $(".taginput-cc .tokenize").hide();

        cc.innerHTML = sendto.innerHTML;
    }
});

$(".taginput-cc input").focus(function ()
{
    cc.remove(sendto.selectedIndex);

});




$("#startTime-datetime").change(function ()
{
    $("#startTimeField").hide();
    $(".form-group.field-startTime .control-label").css({"color": "#000000"})
    var date = stringToDate($("#startTime").val());
    console.log($("#startTime").val());
    if ($("#startTime").val().length > 0)
    {
        var isWk = isWeekend(date);
        if (isWk)
        {
            $("#startTimeField").show();
            $(".form-group.field-startTime .control-label").css({"color": "#a94442"})
        } else {
            $(".form-group.field-startTime .control-label").css({"color": "#3c763d"})
        }
    } else {
        $(".form-group.field-startTime .control-label").css({"color": "#a94442"})
    }


});

$("#endTime-datetime").change(function ()
{

    var date = stringToDate($("#endTime").val());

    $("#endTimeField").hide();
    $(".form-group.field-endTime .control-label").css({"color": "#000000"})
    var date = stringToDate($("#endTime").val());
    console.log($("#startTime").val());
    if ($("#endTime").val().length > 0)
    {
        var isWk = isWeekend(date);
        if (isWk)
        {
            $("#endTimeField").show();
            $(".form-group.field-endTime .control-label").css({"color": "#a94442"})

        } else {
            $(".form-group.field-endTime .control-label").css({"color": "#3c763d"})
        }
    } else {
        $(".form-group.field-endTime .control-label").css({"color": "#a94442"})
    }

});



$('#btn').on('click', function () {

    for (var i = 0; i < sendto.options.length; i++)
    {
        if (sendto.options[i].selected)
        {

            var approver = {id: sendto.options[i].value, name: sendto.options[i].textContent};
            arrSend.push(approver);
        }
    }
    if (arrSend.length == 0)
    {

        $(".taginput-send ul.tokens-container.form-control").css("border", "1px solid #a94442");
        x.style.display = "block";
        $("#send-to-label").css("color", "#a94442")

    } else {
        isfocus = true;
        $(".taginput-send ul.tokens-container.form-control").css("border", "1px solid #000000");
        $("#send-to-label").css("color", "#000000")
        x.style.display = "none"

    }



    if ($("#sendto :selected").length > 0 && !isHide)
    {
        
        for (var i = 0; i < cc.options.length; i++)
        {
            if (cc.options[i].selected)
            {

                var ob = {id: cc.options[i].value, name: cc.options[i].textContent};
                arrCc.push(ob);
            }
              


        }
        
        
    }
     

    var $form = $('#requestForm');

    $form.on('beforeSubmit', function () {
        $.ajax({
            url: $form.attr('action'),
            type: 'POST',
            data: {RequestForm: $('#requestForm').serializeArray(), sendto: arrSend, cc: arrCc},
            error: function (err) {
                console.log(err);
            },
            success: function (res)
            {
                console.log(res);
                if (res)
                {
                    arrCc = [];
                    arrSend = [];
                    window.location.href = $("#btn").attr('value');
                     
                    //   $('#myModalCenter').modal('show');
                }
            }
        });
        return false;
    });








});




function stringToDate($dateString)
{

    if ($dateString.length > 0)
    {
        var parts = $dateString.split(' ');
        var date = parts[0];
        var time = parts[1];
        var type = parts[2];
        var dates = date.split('-');
        var times = time.split(':');
        var hour = Number(times[0]);
        var minute = times[1];


        if (type == 'PM')
        {
            hour += 12;

        }
        var stringD = dates[2] + "-" + dates[1] + "-" + dates[0] + " " + hour + ":" + minute + ":" + "00";

        console.log(stringD);
        var d = new Date(stringD);


        return d;


    }



}

function isWeekend($date) {

    if ($date.getDay() == 6 || $date.getDay() == 0)
    {
        return true;
    }
    return false;
}