var buttons = document.getElementsByClassName('tablinks');
var contents = document.getElementsByClassName('tabcontent');

function showContent(id) {
    var typeGet;
    switch (id)
    {
        case 'tab_progress':
            typeGet = 'inprogress';
            break;
        case 'tab_approved':
            typeGet = 'approved';
            break;
        case 'tab_rejected':
            typeGet = 'rejected';
            break;
    }


    $.ajax({
        url: "manager",
        type: 'POST',
        data: {type: typeGet},
        success: function (response) {
     
            $('#tab-content').html(response);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            
            console.log(textStatus, errorThrown);

        }


    });


}
$(document).ready(function () {
    showContent('tab_progress');
});

for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function () {
        var id = this.name;
        for (var i = 0; i < buttons.length; i++) {
            buttons[i].classList.remove("active");
        }
        this.className += " active";
        showContent(id);
    });
}

