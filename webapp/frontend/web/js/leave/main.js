
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}


var menuBox = document.getElementById('menu-box');
$("#change-lang").hover(function () {

    menuBox.style.display = "block";
}, function () {
    menuBox.style.display = "none";
});


$('#changeEn').click(function () {
    $.ajax({
        url: 'site/changelanguage',
        type: "GET",
        data: {lang: 'en-EN'},
        success: function (result) {
            console.log(result);
            location.reload();
        },
    });
});

$('#changeVi').click(function () {

    $.ajax({
        url: 'site/changelanguage',
        type: "GET",
        data: {lang: 'vi-VI'},
        success: function (result) {
            console.log(result);
            location.reload();
        },
    });
});

$('#show-notify-box').click(function () {

    $('#notify-box').toggle();


});

$(document).mouseup(function (e)
{
    var container = $("#notify-box");
    var menuLeft = $("#mySidenav");
    if (!container.is(e.target) && container.has(e.target).length === 0)
    {

        container.hide(1);
    }
    if (!menuLeft.is(e.target) && menuLeft.has(e.target).length === 0)
    {
        closeNav();
    }
});

