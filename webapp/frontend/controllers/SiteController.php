<?php

namespace frontend\controllers;

use Yii;
use backend\models\User;
use frontend\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\LoginForm;
use common\models\SignupForm;
use common\models\RequestForm;
use backend\models\Stream;
use frontend\common\date\DateTimeFormatter;

/* ;*
 * SiteController implements the CRUD actions for User model.
 */

class SiteController extends Controller {
    /**
     * {@inheritdoc}
     */

    /**
     * Lists all User models.
     * @return mixed
     */
    
 
    public function actionIndex() {

        $this->redirect("leave/default/index");
    }

    public function actionChangelanguage() {

        if (isset($_REQUEST['lang']) && $_REQUEST['lang'] != null) {
            $lang = $_REQUEST['lang'];
            Yii::$app->language = $lang;
            $cookie = new \yii\web\Cookie([
                'name' => 'lang',
                'value' => $_REQUEST['lang'],
            ]);
            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
    }

}
