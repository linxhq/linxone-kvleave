<?php

use yii\web\Request;

$request = new Request();
$baseUrl = str_replace('/frontend/web', '', $request->baseUrl);
$params = array_merge(
        require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php'
);

return [
    'language' => 'en-EN',
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        [
            'class' => 'frontend\modules\leave\components\LanguageSelector',
            'supportedLanguages' => ['en-US', 'vi-VN'],
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'user' => [
            'class' => Da\User\Module::class,
        // ...other configs from here: [Configuration Options](installation/configuration-options.md), e.g.
        // 'generatePasswords' => true,
        // 'switchIdentitySessionKey' => 'myown_usuario_admin_user_key',
        ],
     
        'personal_profiles' => [
            'class' => 'app\modules\leave\site\personal_profiles\Module',
        ],
        'leave' => [
            'class' => 'app\modules\leave\LeaveModule',
        ],
     
    ],
    'components' => [
        'i18n' => [
            'translations' => [
                'frontend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/modules/leave/messages',
                ],
                'backend*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/modules/leave/messages',
                ],
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'frontend\modules\leave\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request' =>
        [
            'baseUrl' => $baseUrl
        ]
        ,
        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'class' => 'yii\rest\UrlRule',
//                '' => 'requests/default/index',
//                'login' => 'users/default/login',
//                'signup' => 'users/default/signup',
//                'logout' => 'users/default/logout',
//                'manager' => 'site/manage',
//                'add' => 'requests/default/add',
//                'view' => 'requests/default/view',
//                'export' => 'statistic/default/export',
//                'accept' => 'requests/default/accept',
//                'approve' => 'requests/default/approve',
            ],
        ],
    ],
    'params' => $params,
];
