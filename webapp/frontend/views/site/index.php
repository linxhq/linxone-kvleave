
    <div class="sea-container">
        <div class="wave"></div>
        <div class="drop">
            <div class="tail"></div>
            <div class="head"></div>
        </div>
        <div class="boat">
            <div class="front"></div>
            <div class="middle">
                <div class="swash"></div>
                <div class="swash"></div>
                <div class="swash"></div>
            </div>
            <div class="back"></div>
            <div class="cabin">
                <div class="hublot"></div>
            </div>
        </div>
    </div>
    <div class="fish">
        <div class="head"></div>
        <div class="body"></div>
        <div class="tail"></div>
        <div class="eye"></div>
    </div>
