<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/leave/main.css',
        'css/leave/login.css',
        'css/leave/tokenize2.css',
        'css/leave/request_form.css',
        'css/leave/statistic.css',
        'css/leave/tabs_manage.css',
        'css/leave/signup.css',
        'css/leave/view_details.css',
        'css/leave/item_leave_list.css',
        'css/leave/item_notify.css',
    ];
    public $js = [
        'js/leave/main.js',
      //  'js/leave/tokenize2.js',
        'js/leave/requestForm.js',
        'js/leave/login.js',
        'js/leave/index.js',
        'js/leave/notify.js',
        'js/leave/statistic.js',
        'js/leave/tabs_manage.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
