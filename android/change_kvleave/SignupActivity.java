package leave.kydon.com.kvleave;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jgabrielfreitas.core.BlurImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.imgBackGround)
    BlurImageView imgBackGround;
    @BindView(R.id.txtLogo)
    TextView txtLogo;
    @BindView(R.id.email)
    AutoCompleteTextView email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.rePassword)
    EditText rePassword;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.email_sign_up_button)
    Button emailSignUpButton;
    @BindView(R.id.lnlLogin)
    LinearLayout lnlLogin;
    @BindView(R.id.txtBack)
    TextView txtBack;
    @BindView(R.id.email_login_form)
    RelativeLayout emailLoginForm;
    @BindView(R.id.prLogin)
    ProgressBar prLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        setBG();

    }

    private void setBG() {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize =4;
        Bitmap bm =BitmapFactory.decodeResource(getResources(),R.drawable.bg1,opts);
        imgBackGround.setImageBitmap(bm);
        imgBackGround.setBlur(7);
        Typeface tf = Typeface.createFromAsset(getAssets(),
                "hvd-neo.ttf");
        txtLogo.setTypeface(tf);
        txtLogo.setTextSize(70f);
    }
}
