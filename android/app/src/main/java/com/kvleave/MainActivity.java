package com.kvleave;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.jgabrielfreitas.core.BlurImageView;

import java.util.Objects;
import java.util.Stack;
import java.util.stream.Stream;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.kvleave.activitystream.ui.StreamDetailsFragment;
import com.kvleave.activitystream.ui.StreamFragment;
import com.kvleave.helper.PrefUtils;
import com.kvleave.manager.ui.ForApproverFragment;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.frameMain)
    FrameLayout frameMain;
    @BindView(R.id.imgBG)
    BlurImageView imgBG;
    @BindView(R.id.drawerlayout)
    FlowingDrawer drawerlayout;



    private FragmentManager fragmentManager;

    private String TAG = getClass().getSimpleName();


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();

        drawerlayout.setOnDrawerStateChangeListener(new ElasticDrawer.OnDrawerStateChangeListener() {
            @Override
            public void onDrawerStateChange(int oldState, int newState) {
                if (newState == ElasticDrawer.STATE_DRAGGING_OPEN) {

                    setupMenu();

                } else if (newState == ElasticDrawer.STATE_CLOSED) {


                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.remove(fragmentManager.findFragmentByTag(MenuListFragment.class.getSimpleName()));
                    fragmentTransaction.commit();
                }
            }

            @Override
            public void onDrawerSlide(float openRatio, int offsetPixels) {

            }
        });


        setBG();

        loadFragment(StreamFragment.newInstance(), true);
        Log.e("SIZE",fragments.size()+"--"+ fragmentManager.getBackStackEntryCount());
        setFirebase();
        getDataNotification();


    }

    public void closeDraw() {
        drawerlayout.closeMenu(true);
;
    }

    public void openDraw() {
        drawerlayout.openMenu(true);
        setupMenu();

    }


    private void setBG() {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 5;
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.bg1, opts);
        imgBG.setImageBitmap(bm);
        imgBG.setBlur(7);
    }

    Stack<Fragment> fragments = new Stack<>();
    public void loadFragment(Fragment frag, boolean transition) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (transition) {
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);

        }

        if (fragments.size() > 0) {

            if (!fragments.peek().getClass().getSimpleName().equals(frag.getClass().getSimpleName())) {

                fragmentTransaction.hide(fragments.peek());

                if (fragmentManager.findFragmentByTag(frag.getClass().getSimpleName()) != null) {
                    Fragment fragment = fragmentManager.findFragmentByTag(frag.getClass().getSimpleName());
                    fragmentTransaction
                            .show(fragment)
                            .commit();

                } else {
                    fragmentTransaction
                            .add(R.id.frameMain, frag, frag.getClass().getSimpleName())
                            .addToBackStack("")
                            .commit();
                  fragments.add(frag);

                }
            }
        } else  {


            fragmentTransaction
                    .add(R.id.frameMain, frag, frag.getClass().getSimpleName())
                    .addToBackStack("")
                    .commit();

               fragments.add(frag);




        }


    }

    private boolean exit = false;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint({"WrongConstant", "ShowToast"})
    @Override
    public void onBackPressed() {
        if (Objects.requireNonNull(fragmentManager.getFragments().get(fragmentManager.getFragments().size() - 1).getTag()).equals(StreamFragment.class.getSimpleName())) {

            Handler handler = new Handler();
            if (exit)
                System.exit(0);
            Toast.makeText(MainActivity.this, "Press again to Exit", 2000).show();
            exit = true;
            handler.postDelayed(new Runnable() {
                @SuppressLint("WrongConstant")
                @Override
                public void run() {
                    exit = false;
                }
            }, 2000);

        } else {

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragments.pop());
            super.onBackPressed();
        }


    }

    public void removeFragment() {

        for (int i = fragments.size(); i > 1; i--) {
            fragments.pop();
            fragmentManager.popBackStack();
        }






    }

    private void setupMenu() {
        MenuListFragment mMenuFragment = (MenuListFragment) fragmentManager.findFragmentById(R.id.id_container_menu);
        if (mMenuFragment == null) {
            mMenuFragment = new MenuListFragment();
            fragmentManager.beginTransaction().add(R.id.id_container_menu, mMenuFragment, MenuListFragment.class.getSimpleName()).commit();


        }

    }
    private void setFirebase() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);

            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }
    }

    public void getDataNotification() {

        if(getIntent().getExtras()!=null)
        {

            String type = getIntent().getExtras().getString("type");
            String streamId = getIntent().getExtras().getString("stream_id");


                switch (type)
                {
                    case "send_request":
                        loadFragment(ForApproverFragment.newInstance(),true);
                        break;
                    case "approved":
                        loadFragment(StreamDetailsFragment.newInstance(streamId),true);
                        break;

                }
        }
    }
}
