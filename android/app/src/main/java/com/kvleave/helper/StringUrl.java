package com.kvleave.helper;

public class StringUrl {
    public static String baseUrl = "https://appdemo.linxhq.com/linxhq_leave/";
    public static String urlLogin = baseUrl + "frontend/web/index.php?r=leave/api/login";
    public static String urlLogout = baseUrl + "frontend/web/index.php?r=leave/api/logout";
    public static String urlSignUp = baseUrl + "frontend/web/index.php?r=leave/api/register";
    public static String urlGetApprover = baseUrl + "frontend/web/index.php?r=leave/api/get-approver";
    public static String urlGetListRequest = baseUrl + "frontend/web/index.php?r=leave/api/get-streams";
    public static String urlSend = baseUrl + "frontend/web/index.php?r=leave/api/add";
    public static String urlGetListInprogress = baseUrl+ "frontend/web/index.php?r=leave/api/get-inprogress";
    public static String urlGetListApproved = baseUrl+ "frontend/web/index.php?r=leave/api/get-approved";
    public static String urlGetListReject= baseUrl+ "frontend/web/index.php?r=leave/api/get-reject";
    public static String urlApprove = baseUrl + "frontend/web/index.php?r=leave/api/approve";
    public static String urlNotify = baseUrl + "frontend/web/index.php?r=leave/api/get-list";
    public static String urlGetStreamById = baseUrl + "frontend/web/index.php?r=leave/api/get";
}
