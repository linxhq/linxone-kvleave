package com.kvleave.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

public class DrawLayout extends View {
    public int width;

    public int height;

    private Bitmap mBitmap;

    private Canvas mCanvas;

    private Path mPath;

    Context context;

    private Paint mPaint;

    private float mX, mY;

    private static final float TOLERANCE = 5;


    public DrawLayout(Context context,int w,int h) {
        super(context);
        init();
        this.width =w;
        this.height =h;
    }

    public DrawLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DrawLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init()
    {
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStrokeWidth(3f);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPaint.setColor(Color.RED);
        mPath.moveTo(0, height/3);
        mPath.lineTo(0, 2*height/3);
        mPath.lineTo(width, height/3);
        mPath.lineTo(width, 0);
        mPath.lineTo(0, height/3);
        canvas.drawPath(mPath,mPaint);

    }


    public void clearCanvas() {
        mPath.reset();
        invalidate();
    }

}
