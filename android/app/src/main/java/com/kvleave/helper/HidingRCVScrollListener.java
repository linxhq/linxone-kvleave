package com.kvleave.helper;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

public abstract class HidingRCVScrollListener extends RecyclerView.OnScrollListener{
    public int offset = 0;
    private int viewHeight;

    public HidingRCVScrollListener(View view) {
        viewHeight = 150;

    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        if(newState == RecyclerView.SCROLL_STATE_IDLE) {
            if (offset > 0 && offset < viewHeight / 2) {
                Log.e("AAAA", offset + "--" + viewHeight / 2);
                onShow();
            } else if (offset > viewHeight / 2 && offset < viewHeight) {

                Log.e("AAAA", offset + "--" + viewHeight / 2);
                onHide();
            }
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        clipToolbarOffset();
        onMoved(offset);

        if((offset <viewHeight && dy>0) || (offset >0 && dy<0)) {
            offset += dy;
        }
    }


    private void clipToolbarOffset() {
        if(offset > viewHeight) {
            offset = viewHeight;
        } else if(offset < 0) {
            offset = 0;
        }
    }

    public abstract void onMoved(int distance);
    public abstract void onShow();
    public abstract void onHide();
}
