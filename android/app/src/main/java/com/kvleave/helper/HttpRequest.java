package com.kvleave.helper;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.MySSLSocketFactory;

import java.security.KeyStore;

/*
 * Created by hadt on 14/08/2017.
 */

public class HttpRequest {
    private static AsyncHttpClient client = null;
    public static AsyncHttpClient getClient(){
        if (client == null) {

            client = new AsyncHttpClient();

            try {
                KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                trustStore.load(null, null);
                MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
                sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                client.setSSLSocketFactory(sf);
            }
            catch (Exception ignored) {
            }
            return client;
        }
        return client;
    }
}
