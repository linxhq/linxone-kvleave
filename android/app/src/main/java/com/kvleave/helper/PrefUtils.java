package com.kvleave.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefUtils {
    /**
     * Storing API Key in shared preferences to
     * add it in header part of every retrofit request
     */
    public PrefUtils() {
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("APP_PREF", Context.MODE_PRIVATE);
    }

    public static void storeTokenUser(Context context, String apiKey) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("TOKEN_USER", apiKey);
        editor.commit();
    }
    public static void storeAccount(Context context,String username,String password)
    {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString("username", username);
        editor.putString("password", password);
        editor.commit();
    }
    public static  String getUsername(Context c)
    {
        return getSharedPreferences(c).getString("username", null);
    }
    public static  String getPassword(Context c)
    {
        return getSharedPreferences(c).getString("password", null);
    }
    public static void clearAll(Context context)
    {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.apply();
    }

    public static String getTokenUser(Context context) {
        return getSharedPreferences(context).getString("TOKEN_USER", null);
    }
}