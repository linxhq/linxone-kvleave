package com.kvleave.helper;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatetimeFormater {
    static @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    public static String format(Calendar calendar)
    {


        return dateFormat.format(calendar.getTime());

    }

    public static Date formatStringToDate(String date) throws ParseException {
        return dateFormat.parse(date);

    }
    public static String formatDateToTime(Date date)
    {

        int hourOfDay =date.getHours();
        int minute =date.getMinutes();

        String time ="";
        String minuteS = "";
        minuteS = minute+"";
        if(minute<10)
        {
            minuteS = "0"+ minute;
        }

        if(hourOfDay > 12) {

            time = String.valueOf(hourOfDay-12)+ ":"+minuteS+" PM";
        } else if(hourOfDay==12) {

            time = "12"+ ":"+minuteS+" PM";
        } else {
            if(hourOfDay!=0) {
                time = String.valueOf(hourOfDay) + ":" + minuteS + " AM";
            } else {
                time = "12" + ":" + minuteS + " AM";
            }
        }
        return time;
    }
    public static String formatDateToString(String date)
    {
        String dateS="";
        try {
            Date  d =formatStringToDate(date);
            SimpleDateFormat dateF=  new SimpleDateFormat("dd/MM/yyyy");

            int hourOfDay =d.getHours();
            int minute =d.getMinutes();

            String time ="";
            String minuteS = "";
            minuteS = minute+"";
            if(minute<10)
            {
                minuteS = "0"+ minute;
            }

            if(hourOfDay > 12) {

                time = String.valueOf(hourOfDay-12)+ ":"+minuteS+" pm";
            } else if(hourOfDay==12) {

                time = "12"+ ":"+minuteS+" pm";
            } else {
                if(hourOfDay!=0) {
                    time = String.valueOf(hourOfDay) + ":" + minuteS + " am";
                } else {
                    time = "12" + ":" + minuteS + " am";
                }
            }
            dateS = dateF.format(d)+" "+time;



        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateS;
    }
    public static String formatString(String date) throws ParseException {

        SimpleDateFormat dateF=  new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
        return dateF.format(formatStringToDate(date));

    }




}
