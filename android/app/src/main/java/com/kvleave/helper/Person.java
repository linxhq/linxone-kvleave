package com.kvleave.helper;

import java.io.Serializable;

public class Person implements Serializable {
    private String id;
    private String name;
    private String email;

    public Person(String id, String n, String e) {
        this.id =id;
        name = n; email = e;

    }
    public Person( String n, String e) {

        name = n; email = e;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() { return name; }
    public String getEmail() { return email; }

    @Override
    public String toString() { return email; }
}