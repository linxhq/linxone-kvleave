package com.kvleave.helper;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

public abstract class HidingScrollListener extends RecyclerView.OnScrollListener{
    private static final int HIDE_THRESHOLD = 20;
    private int scrolledDistance = 0;
    private boolean controlsVisible = true;
    private int offset =0;
    private int dy =0;

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if(newState == RecyclerView.SCROLL_STATE_IDLE)
        {

        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
       // Log.e("DY",dy+"");

        this.dy = dy;

        if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
            onHide();
            controlsVisible = false;
            scrolledDistance = 0;
        } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
            onShow();
            controlsVisible = true;
            scrolledDistance = 0;
        }

        if((controlsVisible && dy>0) || (!controlsVisible && dy<0)) {
            scrolledDistance += dy;



        }
    }




    public abstract void onHide();
    public abstract void onShow();
    public abstract void onIdle(int state);
}
