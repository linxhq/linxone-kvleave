package com.kvleave;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jgabrielfreitas.core.BlurImageView;
import com.kvleave.helper.PrefUtils;
import com.kvleave.login.model.UserLogin;
import com.kvleave.login.presenter.LoginControl;
import com.kvleave.login.presenter.LoginPresenter;
import com.kvleave.login.ui.LoginActivity;
import com.kvleave.notify.models.DataNotify;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements LoginControl {


    @BindView(R.id.txtLogo)
    TextView txtLogo;
    LoginPresenter presenter;
    private String TAG = getClass().getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        setBG();
        presenter = new LoginPresenter(this, this);
        initAccountLogin();

    }






    private void initAccountLogin() {


        if (PrefUtils.getUsername(this) != null && PrefUtils.getPassword(this) != null) {
            presenter.login(PrefUtils.getUsername(this), PrefUtils.getPassword(this), PrefUtils.getTokenUser(this));
        } else {
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            finish();
        }
    }

    private void setBG() {

        Typeface tf = Typeface.createFromAsset(getAssets(),
                "hvd-neo.ttf");
        txtLogo.setTypeface(tf);
        txtLogo.setTextSize(90f);

    }

    @Override
    public void responseLogin(UserLogin userLogin) {

        User.username = userLogin.getData().getUsername();
        User.email = userLogin.getData().getEmail();
        User.id = userLogin.getData().getId();
        User.role = userLogin.getData().getRole();

        Intent intent =new Intent(SplashActivity.this, MainActivity.class);

        if (getIntent().getExtras() != null) {
            String type = getIntent().getExtras().getString("type");
            String streamId = getIntent().getExtras().getString("stream_id");
            if(type!=null && !type.equals(""))
                intent.putExtra("type",type);
            if(streamId!=null && !streamId.equals(""))
            {
                intent.putExtra("stream_id",streamId);
            }


        }
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void responseLogout() {

    }

    @Override
    public void responseFail() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        finish();
    }
}
