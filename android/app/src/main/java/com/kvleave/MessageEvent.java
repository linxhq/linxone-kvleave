package com.kvleave;

import com.kvleave.notify.models.DataNotify;

public class MessageEvent {
    public String message;
    public DataNotify dataNotify;

    public MessageEvent(String message)
    {
        this.message =message;
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataNotify getDataNotify() {
        return dataNotify;
    }

    public void setDataNotify(DataNotify dataNotify) {
        this.dataNotify = dataNotify;
    }
}
