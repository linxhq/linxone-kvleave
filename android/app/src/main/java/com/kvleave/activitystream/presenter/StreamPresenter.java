package com.kvleave.activitystream.presenter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kvleave.activitystream.model.Data;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.activitystream.ui.IStreamRespone;
import com.kvleave.helper.HttpRequest;
import com.kvleave.helper.StringUrl;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;


public class StreamPresenter {
    private Activity activity;
    private IStreamRespone iStreamRespone;
    private String TAG = getClass().getSimpleName();

    private AsyncHttpClient client;
    private Gson gson = new Gson();


    public StreamPresenter(Activity activity, IStreamRespone iView) {
        this.activity = activity;
        this.iStreamRespone = iView;
        client = HttpRequest.getClient();

    }

    public  void getListInprogressr(int id)
    {
        RequestParams params = new RequestParams();
        params.put("id", id);

        client.post(activity, StringUrl.urlGetListInprogress, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                        Data data = gson.fromJson(response.toString(), Data.class);
                        List<StreamOjb> list = data.getData();
                        iStreamRespone.responseData(list);
                        //  Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    } else {

                        iStreamRespone.responseFail();
                        // Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iStreamRespone.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iStreamRespone.responseFail();
            }
        });
    }


    public  void getListApproved(int id)
    {
        RequestParams params = new RequestParams();
        params.put("id", id);

        client.post(activity, StringUrl.urlGetListApproved, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                        Data data = gson.fromJson(response.toString(), Data.class);
                        List<StreamOjb> list = data.getData();
                        iStreamRespone.responseData(list);
                        //  Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    } else {

                        iStreamRespone.responseFail();
                        // Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iStreamRespone.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iStreamRespone.responseFail();
            }
        });
    }

    public  void getListReject(int id)
    {
        RequestParams params = new RequestParams();
        params.put("id", id);

        client.post(activity, StringUrl.urlGetListReject, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                        Data data = gson.fromJson(response.toString(), Data.class);
                        List<StreamOjb> list = data.getData();
                        iStreamRespone.responseData(list);
                        //  Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    } else {

                        iStreamRespone.responseFail();
                        // Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iStreamRespone.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iStreamRespone.responseFail();
            }
        });
    }


    @SuppressLint("CheckResult")
    public void getData(int id) {

        RequestParams params = new RequestParams();
        params.put("id", id);

        client.post(activity, StringUrl.urlGetListRequest, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                        Data data = gson.fromJson(response.toString(), Data.class);
                        List<StreamOjb> list = data.getData();
                        iStreamRespone.responseData(list);
                      //  Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    } else {

                        iStreamRespone.responseFail();
                       // Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iStreamRespone.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iStreamRespone.responseFail();
            }
        });
    }

}
