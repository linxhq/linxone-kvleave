package com.kvleave.activitystream.adapter;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kvleave.R;
import com.kvleave.User;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.activitystream.ui.OnClickItemRcvListener;
import com.kvleave.base.BaseAdapter;
import com.kvleave.base.BaseViewHolder;
import com.kvleave.helper.DatetimeFormater;

import java.text.ParseException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class StreamAdapter extends BaseAdapter<StreamOjb, StreamAdapter.ItemViewHolder> {

    private OnClickItemRcvListener listener;

    public OnClickItemRcvListener getListener() {
        return listener;
    }

    public void setListener(OnClickItemRcvListener listener) {
        this.listener = listener;
    }

    @Override
    protected int itemLayout() {
        return R.layout.item_stream_layout;
    }

    @Override
    protected ItemViewHolder viewHoder(View view) {

        return new ItemViewHolder(view);
    }

    class ItemViewHolder extends BaseViewHolder<StreamOjb> {



        @BindView(R.id.imgAvatar)
        CircleImageView imgAvatar;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.txtTypeLeave)
        TextView txtTypeLeave;
        @BindView(R.id.lnl1)
        LinearLayout lnl1;
        @BindView(R.id.txtRequest)
        TextView txtRequest;
        @BindView(R.id.imgProgress)
        ImageView imgProgress;
        @BindView(R.id.txtProgress)
        TextView txtProgress;
        @BindView(R.id.txtSendApprove)
        TextView txtSendApprove;

        @SuppressLint("SetTextI18n")
        @Override
        protected void bindData(final View view, StreamOjb streamOjb, int viewType, final int position) {

          /*  view.setAlpha(0);
            view.animate().alpha(1).setDuration(500).start();*/


            txtTime.setText(DatetimeFormater.formatDateToString(streamOjb.getTimeStamp()));

            txtName.setText(streamOjb.getUsername());
            txtTypeLeave.setText(getTypeLeave(Integer.parseInt(streamOjb.getLeaveType())));




            if (streamOjb.getApprovedStatus().equals("0") && streamOjb.getApproved().equals("0")) {
                imgProgress.setImageResource(R.mipmap.progress);
                txtProgress.setTextColor(Color.parseColor("#e9923d"));
                txtProgress.setText("In progress");
                //0 0
            } else if (streamOjb.getApprovedStatus().equals("1") &&  streamOjb.getApproved().equals("0")) {
                imgProgress.setImageResource(R.mipmap.reject);
                txtProgress.setTextColor(Color.RED);
                txtProgress.setText("Rejected");
                //1 0

            }else

            if(streamOjb.getApproved()!=null && streamOjb.getApproved().equals("1"))
            {
                imgProgress.setImageResource(R.mipmap.approved);
                txtProgress.setTextColor(Color.GREEN);
                txtProgress.setText("Approved");
                //1
            }


                txtRequest.setText("From " +DatetimeFormater.formatDateToString(streamOjb.getStartTime() )
                        + " to " + DatetimeFormater.formatDateToString(streamOjb.getEndTime()));

            txtSendApprove.setText(streamOjb.getUsername()+" >> "+streamOjb.getApproverName());

            view.setOnClickListener(v -> listener.onClick(view,streamOjb, position));


        }

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    private String getTypeLeave(int type) {
        switch (type) {
            case 1:
                return "Xin nghỉ ốm";
            case 2:
                return "Nghỉ không lương";
            case 3:
                return "Xin nghỉ phép";
            case 4:
                return "Xin nghỉ đẻ/chăm con";

        }
        return "";

    }

}
