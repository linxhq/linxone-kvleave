package com.kvleave.activitystream.ui;

import android.view.View;

import com.kvleave.activitystream.model.StreamOjb;

public interface OnClickItemRcvListener {
    void onClick(View view, StreamOjb streamOjb, int position);
}
