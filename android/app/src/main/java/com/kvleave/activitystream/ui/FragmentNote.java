package com.kvleave.activitystream.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.kvleave.MainActivity;
import com.kvleave.R;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.base.BaseFragment;

public class FragmentNote extends BaseFragment {


    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.toolBar)
    RelativeLayout toolBar;
    @BindView(R.id.btnContinue)
    CardView btnContinue;
    private StreamOjb streamOjb;
    @BindView(R.id.edtNote)
    EditText edtNote;



    @Override
    protected int idLayout() {
        return R.layout.note_fragment;
    }

    @Override
    protected int idBackButton() {
        return R.id.btnBack;
    }

    public static FragmentNote newInstance(StreamOjb streamOjb) {

        Bundle args = new Bundle();

        FragmentNote fragment = new FragmentNote();
        fragment.setArguments(args);
        fragment.streamOjb=streamOjb;
        return fragment;
    }



    @OnClick(R.id.btnContinue)
    public void onViewClicked() {
        String note = edtNote.getText().toString();
        streamOjb.setNote(note);
        ((MainActivity)getActivity()).loadFragment(ApproveFragment.newInstance(streamOjb),true);
    }

}
