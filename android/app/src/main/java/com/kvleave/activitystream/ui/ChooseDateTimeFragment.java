package com.kvleave.activitystream.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.datetimepicker.date.DatePickerDialog;
import com.android.datetimepicker.time.RadialPickerLayout;
import com.android.datetimepicker.time.TimePickerDialog;
import com.google.gson.Gson;
import com.jgabrielfreitas.core.BlurImageView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.kvleave.MainActivity;
import com.kvleave.R;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.base.BaseFragment;
import com.kvleave.helper.DatetimeFormater;

public class ChooseDateTimeFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {
    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.toolBar)
    RelativeLayout toolBar;
    @BindView(R.id.txtDateStart)
    TextView txtDateStart;
    @BindView(R.id.txtTimeStart)
    TextView txtTimeStart;
    @BindView(R.id.txtDateEnd)
    TextView txtDateEnd;
    @BindView(R.id.txtTimeEnd)
    TextView txtTimeEnd;
    @BindView(R.id.btnContinue)
    CardView btnContinue;


    private DatePickerDialog pickerDialog;
    private Calendar calendarStart, calendarEnd, calendarMin;
    private final String TIME_PATTERN = "hh:mm a";
    private final String DATE_PATTERN = "dd/MM/yyyy";
    private SimpleDateFormat dateFormat, timeFormat;
    private boolean isStartDate = false, isStartTime = false;
    private StreamOjb streamOjb;
    @Override
    protected int idLayout() {
        return R.layout.date_time_fragment;
    }

    @Override
    protected int idBackButton() {
        return R.id.btnBack;
    }

    public static ChooseDateTimeFragment newInstance(StreamOjb streamOjb) {

        Bundle args = new Bundle();
        ChooseDateTimeFragment fragment = new ChooseDateTimeFragment();
        fragment.setArguments(args);
        fragment.streamOjb = streamOjb;

        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // pickerDialog = new DatePickerDialog();
        //  setBG();
        calendarStart = Calendar.getInstance();
        calendarEnd = Calendar.getInstance();
        calendarMin = Calendar.getInstance();
        dateFormat = new SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH);
        timeFormat = new SimpleDateFormat(TIME_PATTERN, Locale.ENGLISH);
        intTimeTextView();
    }



    private void intTimeTextView() {
        txtDateStart.setText(dateFormat.format(calendarStart.getTime()));
        txtDateEnd.setText(dateFormat.format(calendarEnd.getTime()));
        txtTimeStart.setText(DatetimeFormater.formatDateToTime(calendarStart.getTime()));
        txtTimeEnd.setText(DatetimeFormater.formatDateToTime(calendarEnd.getTime()));
    }


    @OnClick({R.id.txtDateStart, R.id.txtTimeStart, R.id.txtDateEnd, R.id.txtTimeEnd,R.id.btnContinue})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.txtDateStart:
                isStartDate = true;
                pickerDialog = DatePickerDialog.newInstance(this, calendarStart.get(Calendar.YEAR), calendarStart.get(Calendar.MONTH), calendarStart.get(Calendar.DAY_OF_MONTH));
                pickerDialog.setMinDate(Calendar.getInstance());
                pickerDialog.show(getActivity().getFragmentManager(), "datePickerStart");


                break;
            case R.id.txtTimeStart:
                isStartTime = true;
                TimePickerDialog.newInstance(this, calendarStart.get(Calendar.HOUR_OF_DAY), calendarStart.get(Calendar.MINUTE), true).show(getActivity().getFragmentManager(), "timePickerStart");

                break;
            case R.id.txtDateEnd:

                isStartDate = false;
                pickerDialog = DatePickerDialog.newInstance(this, calendarMin.get(Calendar.YEAR), calendarMin.get(Calendar.MONTH), calendarMin.get(Calendar.DAY_OF_MONTH));
                pickerDialog.setMinDate(calendarStart);
                pickerDialog.show(getActivity().getFragmentManager(), "datePickerEnd");
                break;
            case R.id.txtTimeEnd:
                isStartTime = false;
                TimePickerDialog.newInstance(this, calendarEnd.get(Calendar.HOUR_OF_DAY), calendarEnd.get(Calendar.MINUTE), true).show(getActivity().getFragmentManager(), "timePickerEnd");

                break;
            case R.id.btnContinue:

                streamOjb.setStartTime(DatetimeFormater.format(calendarStart));
                streamOjb.setEndTime(DatetimeFormater.format(calendarEnd));
              //  Log.e("OBJ ",new Gson().toJson(streamOjb));

                ((MainActivity)getActivity()).loadFragment(FragmentNote.newInstance(streamOjb),true);
                break;
        }
    }


    @Override
    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
        if (isStartDate) {
            calendarStart.set(year, monthOfYear, dayOfMonth);
            calendarMin.set(year, monthOfYear, dayOfMonth);
            txtDateStart.setText(dateFormat.format(calendarStart.getTime()));
            txtDateEnd.setText(dateFormat.format(calendarStart.getTime()));


        } else {
            calendarMin.set(year, monthOfYear, dayOfMonth);
            calendarEnd.set(year, monthOfYear, dayOfMonth);
            txtDateEnd.setText(dateFormat.format(calendarEnd.getTime()));
        }

    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        if (isStartTime) {
            calendarStart.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarStart.set(Calendar.MINUTE, minute);
            txtTimeStart.setText(timeFormat.format(calendarStart.getTime()));
        } else {
            calendarEnd.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendarEnd.set(Calendar.MINUTE, minute);
            txtTimeEnd.setText(timeFormat.format(calendarEnd.getTime()));
        }

    }



}
