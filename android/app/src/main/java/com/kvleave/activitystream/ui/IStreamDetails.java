package com.kvleave.activitystream.ui;

import com.kvleave.activitystream.model.StreamOjb;

public interface IStreamDetails {
    void responseData(StreamOjb streamOjb);

    void responseFail();
}
