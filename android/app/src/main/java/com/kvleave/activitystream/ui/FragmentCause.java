package com.kvleave.activitystream.ui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jgabrielfreitas.core.BlurImageView;

import butterknife.BindView;
import butterknife.OnClick;
import com.kvleave.MainActivity;
import com.kvleave.R;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.base.BaseFragment;

public class FragmentCause extends BaseFragment {


    @BindView(R.id.btnBack)
    ImageView btnBack;

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.toolBar)
    RelativeLayout toolBar;
    @BindView(R.id.txtType1)
    TextView txtType1;
    @BindView(R.id.lnlType1)
    CardView lnlType1;
    @BindView(R.id.txtType2)
    TextView txtType2;
    @BindView(R.id.lnlType2)
    CardView lnlType2;
    @BindView(R.id.txtType3)
    TextView txtType3;
    @BindView(R.id.lnlType3)
    CardView lnlType3;
    @BindView(R.id.txtType4)
    TextView txtType4;
    @BindView(R.id.lnlType4)
    CardView lnlType4;
    private StreamOjb streamOjb;


    @Override
    protected int idLayout() {
        return R.layout.cause_layout;
    }

    @Override
    protected int idBackButton() {
        return R.id.btnBack;
    }
/*
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        DrawLayout drawView = new DrawLayout(getActivity(),width,height);
        drawView.setBackgroundColor(Color.WHITE);
        return drawView;

    }*/


    public static FragmentCause newInstance() {

        Bundle args = new Bundle();

        FragmentCause fragment = new FragmentCause();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        streamOjb = new StreamOjb();
    }





    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick({R.id.txtType1,R.id.txtType2, R.id.txtType3, R.id.txtType4})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtType1:
                streamOjb.setLeaveType("1");
                Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.fly);
                lnlType1.startAnimation(anim);
              //  lnlType1.setBackgroundResource(R.drawable.demo);
                ((MainActivity)getActivity()).loadFragment(ChooseDateTimeFragment.newInstance(streamOjb),true);
                break;


            case R.id.txtType2:
                streamOjb.setLeaveType("2");
                lnlType2.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fly));
                ((MainActivity)getActivity()).loadFragment(ChooseDateTimeFragment.newInstance(streamOjb),true);
                break;

            case R.id.txtType3:
                streamOjb.setLeaveType("3");
          lnlType3.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fly));

                ((MainActivity)getActivity()).loadFragment(ChooseDateTimeFragment.newInstance(streamOjb),true);

                break;

            case R.id.txtType4:
                streamOjb.setLeaveType("4");
                lnlType4.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fly));

                ((MainActivity)getActivity()).loadFragment(ChooseDateTimeFragment.newInstance(streamOjb),true);
                break;

        }
    }
}
