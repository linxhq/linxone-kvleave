package com.kvleave.activitystream.ui;

import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.login.model.User;

import java.util.List;

public interface IApproveResponse {
    void responseData(List<User> users);
    void responseData(StreamOjb streamOjb);
    void responseSendSuccess(String s);
    void responseFail();
}
