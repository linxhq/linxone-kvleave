package com.kvleave.activitystream.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.kvleave.MainActivity;
import com.kvleave.MessageEvent;
import com.kvleave.R;
import com.kvleave.User;
import com.kvleave.activitystream.adapter.StreamAdapter;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.activitystream.presenter.StreamPresenter;
import com.kvleave.manager.ui.ForApproverFragment;
import com.kvleave.base.BaseFragment;
import com.kvleave.helper.HidingScrollListener;
import com.kvleave.notify.models.DataNotify;
import com.kvleave.notify.ui.NotifyFrament;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

public class StreamFragment extends BaseFragment implements OnClickItemRcvListener, IStreamRespone, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.imgListLeave)
    ImageView imgListLeave;
    @BindView(R.id.txtNotifyNumber)
    TextView txtNotifyNumber;
    @BindView(R.id.toolBar)
    FrameLayout toolBar;
    @BindView(R.id.rcvStream)
    RecyclerView rcvStream;
    @BindView(R.id.sw)
    SwipeRefreshLayout sw;
    @BindView(R.id.fl)
    FrameLayout fl;
    @BindView(R.id.btnAdd)
    CardView btnAdd;
    @BindView(R.id.txtEmpty)
    TextView txtEmpty;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.imgMenu)
    ImageView imgMenu;

    private StreamPresenter presenter;

    private StreamAdapter adapter;
    private List<StreamOjb> list;
    int num =0;

    @Override
    protected int idLayout() {
        return R.layout.stream_layout;
    }

    @Override
    protected int idBackButton() {
        return 0;
    }


    public static StreamFragment newInstance() {
        StreamFragment fragment = new StreamFragment();
        return fragment;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initRecyclerView();
        setData();

        //setDataListView();
        presenter = new StreamPresenter(getActivity(), this);
        presenter.getData(User.id);
        sw.setOnRefreshListener(this);
    }


    private void initRecyclerView() {
        RecyclerView.RecycledViewPool mSharedPool = new RecyclerView.RecycledViewPool();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcvStream.setLayoutManager(linearLayoutManager);
        linearLayoutManager.setInitialPrefetchItemCount(10);
        rcvStream.setLayoutManager(linearLayoutManager);
        rcvStream.setRecycledViewPool(mSharedPool);
        setRecyclerView(rcvStream, true, 0, false);

        View view = toolBar;
        rcvStream.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                view.animate().translationY(-view.getHeight()).setInterpolator(new AccelerateInterpolator(2));


                btnAdd.animate().translationY(btnAdd.getHeight() + pxFromDp(getActivity(), 20f)).setInterpolator(new AccelerateInterpolator(2));
            }


            @Override
            public void onShow() {
                view.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
                btnAdd.animate().translationY(0).setInterpolator(new AccelerateInterpolator(2));
            }

            @Override
            public void onIdle(int state) {


            }


        });
    }

    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    private void setData() {

        list = new ArrayList<>();
        adapter = new StreamAdapter();
        adapter.setData(list);
        adapter.setListener(this);
        AlphaInAnimationAdapter alpha = new AlphaInAnimationAdapter(adapter);

        alpha.setFirstOnly(false);
        rcvStream.setAdapter(alpha);

        //   rcvStream.setAdapter(alphaAdapter);
        //rcvStream.setAdapter(adapter);
    }


    @Override
    public void onClick(View view, StreamOjb item, int position) {
        ((MainActivity) getActivity()).loadFragment(StreamDetailsFragment.newInstance(item.getId()), true);

    }

    @Override
    public void responseData(List<StreamOjb> list) {
        sw.setRefreshing(false);
        if (list.size() == 0) {
            txtEmpty.setVisibility(View.VISIBLE);
            adapter.setEmptyData();
            return;

        } else txtEmpty.setVisibility(View.GONE);

        adapter.setData(list);


    }

    @Override
    public void responseFail() {

        txtEmpty.setVisibility(View.VISIBLE);
        sw.setRefreshing(false);
        adapter.setEmptyData();
    }

    @Override
    public void onRefresh() {
        presenter.getData(User.id);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.message.equals("update_activity")) {
            presenter.getData(User.id);
        }else if(event.message.equals("notification"))
        {
            num +=1;
            txtNotifyNumber.setText(String.valueOf(num));

            DataNotify dataNotify = event.getDataNotify();
            if(dataNotify.getType().equals("approved"))
            {
                presenter.getData(User.id);
            }
        }

    }


    @OnClick({R.id.imgListLeave, R.id.btnAdd,R.id.imgMenu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgListLeave:
                num=0;
                txtNotifyNumber.setText(String.valueOf(num));
                ((MainActivity) getActivity()).loadFragment(NotifyFrament.newInstance(), true);
                break;
            case R.id.btnAdd:
                ((MainActivity) getActivity()).loadFragment(FragmentCause.newInstance(), true);
                break;
            case R.id.imgMenu:
                ((MainActivity) getActivity()).openDraw();
                break;
        }
    }

    ;


}
