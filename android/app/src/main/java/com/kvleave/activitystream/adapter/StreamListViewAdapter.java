package com.kvleave.activitystream.adapter;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kvleave.R;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.activitystream.ui.OnClickItemRcvListener;
import com.kvleave.base.BaseListViewAdapter;
import com.kvleave.base.BaseListviewViewHolder;
import com.kvleave.helper.DatetimeFormater;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class  StreamListViewAdapter extends BaseListViewAdapter<StreamOjb, StreamListViewAdapter.ViewHolder> {


    @Override
    protected int getViewId() {
        return R.layout.item_stream_layout;
    }

    @Override
    protected ViewHolder viewHolder(View view) {
        return new ViewHolder(view);
    }



    public class ViewHolder extends BaseListviewViewHolder<StreamOjb> {

        @BindView(R.id.imgProgress)
        ImageView imgProgress;
        @BindView(R.id.txtProgress)
        TextView txtProgress;
        @BindView(R.id.imgAvatar)
        CircleImageView imgAvatar;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.txtTypeLeave)
        TextView txtTypeLeave;
        @BindView(R.id.lnl1)
        LinearLayout lnl1;
        @BindView(R.id.txtRequest)
        TextView txtRequest;
        @BindView(R.id.txtSendApprove)
        TextView txtSendApprove;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void binData(StreamOjb streamOjb, View view, int position) {


            txtTime.setText(DatetimeFormater.formatDateToString(streamOjb.getTimeStamp()));

            txtName.setText(streamOjb.getUsername());
            txtTypeLeave.setText(getTypeLeave(Integer.parseInt(streamOjb.getLeaveType())));


            if (streamOjb.getApprovedStatus().equals("0") && streamOjb.getApproved().equals("0")) {
                imgProgress.setImageResource(R.mipmap.progress);
                txtProgress.setTextColor(Color.parseColor("#e9923d"));
                txtProgress.setText("In progress");
            } else if (streamOjb.getApprovedStatus().equals("1") &&  streamOjb.getApproved().equals("0")) {
                imgProgress.setImageResource(R.mipmap.reject);
                txtProgress.setTextColor(Color.RED);
                txtProgress.setText("Rejected");

            }else

            if(streamOjb.getApproved()!=null && streamOjb.getApproved().equals("1"))
            {
                imgProgress.setImageResource(R.mipmap.approved);
                txtProgress.setTextColor(Color.GREEN);
                txtProgress.setText("Approved");
            }


            txtRequest.setText("From " + DatetimeFormater.formatDateToString(streamOjb.getStartTime() )
                    + " to " + DatetimeFormater.formatDateToString(streamOjb.getEndTime()));

            txtSendApprove.setText(streamOjb.getUsername()+" >> "+streamOjb.getApproverName());



        }
    }

    private String getTypeLeave(int type) {
        switch (type) {
            case 1:
                return "Xin nghỉ ốm";
            case 2:
                return "Nghỉ không lương";
            case 3:
                return "Xin nghỉ phép";
            case 4:
                return "Xin nghỉ đẻ/chăm con";

        }
        return "";

    }

}
