package com.kvleave.activitystream.ui;

import java.util.List;

import com.kvleave.activitystream.model.StreamOjb;

public interface IStreamRespone {

    void responseData(List<StreamOjb> list);
    void responseFail();
}
