package com.kvleave.activitystream.presenter;

import android.app.Activity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kvleave.activitystream.model.Approver;
import com.kvleave.activitystream.model.Data;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.activitystream.ui.IApproveResponse;
import com.kvleave.activitystream.ui.IStreamRespone;
import com.kvleave.helper.HttpRequest;
import com.kvleave.helper.StringUrl;
import com.kvleave.login.model.User;
import com.kvleave.login.model.UserLogin;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.stream.Stream;

import cz.msebera.android.httpclient.Header;

public class ApprovedPresenter {

    private Activity activity;
    private IApproveResponse iApproveResponse;
    private String TAG = getClass().getSimpleName();

    private AsyncHttpClient client;
    private Gson gson = new Gson();


    public ApprovedPresenter(Activity activity, IApproveResponse iView) {
        this.activity = activity;
        this.iApproveResponse = iView;
        client = HttpRequest.getClient();

    }

    public void getApprover()
    {

        RequestParams params = new RequestParams();
        client.post(activity, StringUrl.urlGetApprover, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                iApproveResponse.responseFail();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iApproveResponse.responseFail();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                iApproveResponse.responseFail();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                        Approver data = gson.fromJson(response.toString(), Approver.class);
                        List<User> list = data.getData();
                        iApproveResponse.responseData(list);
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    } else {

                        iApproveResponse.responseFail();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iApproveResponse.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iApproveResponse.responseFail();
            }
        });
    }

    public void doApprove(String id,String type)
    {
        RequestParams params = new RequestParams();
        params.put("id",id);
        if(type.equals("approved")) {
            params.put("approved", "1");
            params.put("approved_status", "0");
            params.put("approver_id", com.kvleave.User.id);
        }
        else if(type.equals("reject")) {

            params.put("approved", "0");
            params.put("approved_status", "1");
            params.put("approver_id", com.kvleave.User.id);

        }
        client.post(activity, StringUrl.urlApprove, params, new JsonHttpResponseHandler() {


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iApproveResponse.responseFail();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                iApproveResponse.responseFail();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {

                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                        if(type.equals("approved"))
                            iApproveResponse.responseSendSuccess("approved");
                        else  iApproveResponse.responseSendSuccess("reject");

                    } else {

                        iApproveResponse.responseFail();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iApproveResponse.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iApproveResponse.responseFail();
            }
        });

    }
    public void sendRequest(StreamOjb streamOjb) {
        RequestParams params = new RequestParams();
        params.put("user_id", com.kvleave.User.id);
        params.put("start_time",streamOjb.getStartTime());
        params.put("end_time", streamOjb.getEndTime());
        params.put("leave_type", streamOjb.getLeaveType());
        params.put("note",  streamOjb.getNote() );
        params.put("cc",  streamOjb.getCc() );
        params.put("approver_id",  streamOjb.getApproverId() );
        client.post(activity, StringUrl.urlSend, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                iApproveResponse.responseFail();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iApproveResponse.responseFail();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                iApproveResponse.responseFail();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {

                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                        iApproveResponse.responseSendSuccess("");

                    } else {

                        iApproveResponse.responseFail();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iApproveResponse.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iApproveResponse.responseFail();
            }
        });
    }

    public void getStreamById(String id)
    {

        RequestParams params = new RequestParams();
        params.put("id",id);
        client.post(activity, StringUrl.urlGetStreamById, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                iApproveResponse.responseFail();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iApproveResponse.responseFail();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                iApproveResponse.responseFail();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {

                        StreamOjb data = gson.fromJson(response.getString("data"), StreamOjb.class);

                        iApproveResponse.responseData(data);
                        // Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    } else {

                        iApproveResponse.responseFail();
                        //  Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iApproveResponse.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iApproveResponse.responseFail();
            }
        });
    }


}
