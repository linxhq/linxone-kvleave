package com.kvleave.activitystream.ui;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kvleave.MainActivity;
import com.kvleave.MessageEvent;
import com.kvleave.R;
import com.kvleave.User;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.activitystream.presenter.ApprovedPresenter;
import com.kvleave.base.BaseFragment;
import com.kvleave.helper.DatetimeFormater;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class StreamDetailsFragment extends BaseFragment implements IApproveResponse {
    @BindView(R.id.btnBack)
    ImageView btnBack;
    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.toolBar)
    RelativeLayout toolBar;
    @BindView(R.id.txtType)
    TextView txtType;
    @BindView(R.id.txtName)
    TextView txtName;
    @BindView(R.id.txtStartTime)
    TextView txtStartTime;
    @BindView(R.id.txtEndTime)
    TextView txtEndTime;
    @BindView(R.id.txtCause)
    TextView txtCause;
    @BindView(R.id.txtStatus)
    TextView txtStatus;
    @BindView(R.id.btnReject)
    Button btnReject;
    @BindView(R.id.btnApprove)
    Button btnApprove;
    @BindView(R.id.lnlApprove)
    LinearLayout lnlApprove;


    private ApprovedPresenter presenter;


    String id;
    @Override
    protected int idLayout() {
        return R.layout.fragment_stream_details;
    }

    @Override
    protected int idBackButton() {
        return R.id.btnBack;
    }

    public static StreamDetailsFragment newInstance(String id) {
        Bundle args = new Bundle();
        StreamDetailsFragment fragment = new StreamDetailsFragment();
        fragment.setArguments(args);
        fragment.id = id;
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new ApprovedPresenter(getActivity(),this);
        getData();

    }

    private void setData(StreamOjb streamOjb) {


        txtType.setText(getTypeLeave(Integer.parseInt(streamOjb.getLeaveType())));

        txtName.setText(streamOjb.getUsername());
        txtCause.setText(streamOjb.getNote());

        txtStartTime.setText(DatetimeFormater.formatDateToString(streamOjb.getStartTime()));
        txtEndTime.setText(DatetimeFormater.formatDateToString(streamOjb.getEndTime()));

        if (streamOjb.getApprovedStatus().equals("0") && streamOjb.getApproved().equals("0")) {

            if (streamOjb.getApproverId() == User.id ) {
                lnlApprove.setVisibility(View.VISIBLE);
            }
            txtStatus.setTextColor(Color.parseColor("#e9923d"));
            txtStatus.setText("In progress");
        } else if (streamOjb.getApprovedStatus().equals("1") && streamOjb.getApproved().equals("0")) {

            txtStatus.setTextColor(Color.RED);
            txtStatus.setText("Rejected");

        } else if (streamOjb.getApproved() != null && streamOjb.getApproved().equals("1")) {

            txtStatus.setTextColor(Color.GREEN);
            txtStatus.setText("Approved");
        }


    }

    private String getTypeLeave(int type) {
        switch (type) {
            case 1:
                return "Xin nghỉ ốm";
            case 2:
                return "Nghỉ không lương";
            case 3:
                return "Xin nghỉ phép";
            case 4:
                return "Xin nghỉ đẻ/chăm con";

        }
        return "";

    }



    @OnClick({R.id.btnReject, R.id.btnApprove})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnReject:
                presenter.doApprove(id,"reject");
                break;
            case R.id.btnApprove:
                presenter.doApprove(id,"approved");
                break;
        }
    }

    @Override
    public void responseData(List<com.kvleave.login.model.User> users) {
        // not used
    }

    @Override
    public void responseData(StreamOjb streamOjb) {
        setData(streamOjb);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void responseSendSuccess(String type) {
        if(type.equals("approved"))
        EventBus.getDefault().post(new MessageEvent("update_list_approved"));
        else if(type.equals("reject"))
            EventBus.getDefault().post(new MessageEvent("update_list_reject"));
        ((MainActivity)getActivity()).onBackPressed();

    }

    @Override
    public void responseFail() {

    }

    public void getData() {
        if(id!=null)
        presenter.getStreamById(id);

    }
}
