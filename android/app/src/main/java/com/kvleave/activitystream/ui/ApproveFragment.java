package com.kvleave.activitystream.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kvleave.MessageEvent;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.activitystream.presenter.ApprovedPresenter;
import com.kvleave.helper.ContactsCompletionView;
import com.kvleave.helper.Person;
import com.kvleave.login.model.User;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.pchmn.materialchips.ChipsInput;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import com.kvleave.MainActivity;
import com.kvleave.R;
import com.kvleave.activitystream.model.EmailContact;
import com.kvleave.base.BaseFragment;
import com.tokenautocomplete.TokenCompleteTextView;
import com.tylersuehr.chips.ChipsInputLayout;

import org.greenrobot.eventbus.EventBus;

public class ApproveFragment extends BaseFragment implements IApproveResponse {

    @BindView(R.id.txtTitle)
    TextView txtTitle;
    @BindView(R.id.toolBar)
    RelativeLayout toolBar;
    @BindView(R.id.input_email_send)
    ContactsCompletionView input_email_send;
    @BindView(R.id.imgComplete)
    ImageView imgComplete;
    @BindView(R.id.input_email_cc)
    ContactsCompletionView input_email_cc;
    @BindView(R.id.layoutEmail)
    LinearLayout layoutEmail;
    @BindView(R.id.btnComplete)
    CardView btnComplete;
    @BindView(R.id.prComplete)
    CircularProgressBar prComplete;
    private List<Person> emailContacts;
    private ArrayList<Person> emailContactsSelected;
    private ArrayList<Person> ccContactsSelected;

    private boolean isComplete = false;
    private StreamOjb streamOjb;
    private ApprovedPresenter presenter;

    @Override
    protected int idLayout() {
        return R.layout.approve_fragment;
    }

    @Override
    protected int idBackButton() {
        return R.id.btnBack;
    }

    public static ApproveFragment newInstance(StreamOjb streamOjb) {

        Bundle args = new Bundle();

        ApproveFragment fragment = new ApproveFragment();
        fragment.setArguments(args);
        fragment.streamOjb = streamOjb;
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new ApprovedPresenter(getActivity(), this);
        presenter.getApprover();
        selectEmail();
    }


    private void selectEmail() {
        emailContactsSelected = new ArrayList();
        ccContactsSelected = new ArrayList();
        input_email_send.setTokenListener(new TokenCompleteTextView.TokenListener<Person>() {
            @Override
            public void onTokenAdded(Person token) {
                emailContactsSelected.add(token);
            }

            @Override
            public void onTokenRemoved(Person token) {
                emailContactsSelected.remove(token);
            }
        });
        input_email_cc.setTokenListener(new TokenCompleteTextView.TokenListener<Person>() {
            @Override
            public void onTokenAdded(Person token) {
                ccContactsSelected.add(token);
            }

            @Override
            public void onTokenRemoved(Person token) {
                ccContactsSelected.remove(token);
            }
        });
    }

    boolean isSuccess = true;

    @OnClick(R.id.btnComplete)
    public void onViewClicked() {

        if (!isComplete) {
            if (emailContactsSelected.size() > 0) {

                Person person = emailContactsSelected.get(0);

                if (person.getId() != null)

                {
                    streamOjb.setApproverId(Integer.parseInt(person.getId()));

                    String cc = "";
                    for (int i = 0; i < ccContactsSelected.size(); i++) {
                        if (ccContactsSelected.get(i).getId() != null) {
                            if (!ccContactsSelected.get(i).getId().equals(person.getId()))
                                cc = cc + "," + ccContactsSelected.get(i).getId();
                            isSuccess = true;
                        } else {
                            Toast.makeText(getActivity(), ccContactsSelected.get(i).getEmail() + " not exist", Toast.LENGTH_SHORT).show();
                            isSuccess = false;
                            break;
                        }
                    }
                    if (isSuccess) {
                        if (cc.length() > 0) {
                            String ccNew = cc.substring(1, cc.length());
                            streamOjb.setCc(ccNew);
                        }


                        Log.e("OBJ", new Gson().toJson(streamOjb));

                        presenter.sendRequest(streamOjb);
                        layoutEmail.setVisibility(View.GONE);
                        prComplete.setVisibility(View.VISIBLE);
                        btnComplete.setVisibility(View.GONE);
                    }
                } else {

                    Toast.makeText(getActivity(), "User not exist", Toast.LENGTH_SHORT).show();

                }
            } else {
                Toast.makeText(getActivity(), "Choose email to send request", Toast.LENGTH_SHORT).show();
            }

        } else {
            {
                EventBus.getDefault().post(new MessageEvent("update_activity"));
                ((MainActivity) getActivity()).removeFragment();

            }


        }

    }


    @Override
    public void responseData(List<User> users) {
        emailContacts = new ArrayList<>();

        for (int i = 0; i < users.size(); i++) {
            if(!users.get(i).getId().equals(com.kvleave.User.id))
            emailContacts.add(new Person(users.get(i).getId() + "", users.get(i).getUsername(), users.get(i).getEmail()));
        }


        // ChipsInputLayout.setFilterableChipList(emailContacts);
        if (getActivity() != null) {
            ArrayAdapter adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, emailContacts);
            input_email_send.setAdapter(adapter);
            input_email_cc.setAdapter(adapter);
        }


    }

    @Override
    public void responseData(StreamOjb streamOjb) {

    }


    @Override
    public void responseSendSuccess(String type) {
        isComplete = true;
        prComplete.setVisibility(View.GONE);
        imgComplete.setVisibility(View.VISIBLE);
        btnComplete.setVisibility(View.VISIBLE);


    }

    @Override
    public void responseFail() {

    }


}
