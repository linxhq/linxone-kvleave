package com.kvleave.activitystream.model;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pchmn.materialchips.model.ChipInterface;
import com.tylersuehr.chips.Chip;

public class EmailContact extends Chip {
    private String id;
    private Uri avatarUri;
    private String name;
    private String phoneNumber;

    public EmailContact(String id, String name, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String getId() {
        return id;
    }

    @NonNull
    @Override
    public String getTitle() {
        return name;
    }

    @Nullable
    @Override
    public String getSubtitle() {
        return phoneNumber;
    }

    @Nullable
    @Override
    public Uri getAvatarUri() {
        return null;
    }

    @Nullable
    @Override
    public Drawable getAvatarDrawable() {
        return null;
    }


}
