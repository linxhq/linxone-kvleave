package com.kvleave.manager.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class PagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragments;
    private String[] title;

    public PagerAdapter(FragmentManager fm,List<Fragment> fragments, String[] title) {
        super(fm);
        this.fragments = fragments;
        this.title =title;
    }


    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        return title[position];
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }


}
