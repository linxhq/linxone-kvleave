package com.kvleave.manager.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kvleave.MainActivity;
import com.kvleave.MessageEvent;
import com.kvleave.R;
import com.kvleave.User;
import com.kvleave.activitystream.adapter.StreamAdapter;
import com.kvleave.activitystream.model.StreamOjb;
import com.kvleave.activitystream.presenter.StreamPresenter;
import com.kvleave.activitystream.ui.IStreamRespone;
import com.kvleave.activitystream.ui.OnClickItemRcvListener;
import com.kvleave.activitystream.ui.StreamDetailsFragment;
import com.kvleave.base.BaseFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class TabRejectFragment extends BaseFragment implements IStreamRespone, OnClickItemRcvListener, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.rcvListReject)
    RecyclerView rcvListReject;
    @BindView(R.id.sw)
    SwipeRefreshLayout sw;
    @BindView(R.id.txtEmpty)
    TextView txtEmpty;
    private StreamAdapter adapter;
    private List<StreamOjb> list;
    private StreamPresenter presenter;

    @Override
    protected int idLayout() {
        return R.layout.fragment_reject;
    }

    @Override
    protected int idBackButton() {
        return 0;
    }

    public static TabRejectFragment newInstance() {


        TabRejectFragment fragment = new TabRejectFragment();

        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new StreamPresenter(getActivity(), this);
        presenter.getListReject(User.id);
        sw.setOnRefreshListener(this);
        setData();
        txtEmpty.setText("Empty");
    }

    private void setData() {
        list = new ArrayList<>();
        adapter = new StreamAdapter();
        adapter.setListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcvListReject.setLayoutManager(linearLayoutManager);
        rcvListReject.setAdapter(adapter);
    }

    @Override
    public void onRefresh() {
        presenter.getListReject(User.id);
    }

    @Override
    public void responseData(List<StreamOjb> list) {
        if (sw != null)
            sw.setRefreshing(false);

        if (txtEmpty != null) {
            if (list.size() == 0) {
                txtEmpty.setVisibility(View.VISIBLE);
                adapter.setEmptyData();
                return;
            } else txtEmpty.setVisibility(View.GONE);
        }

        adapter.setData(list);
    }

    @Override
    public void responseFail() {
        if (sw != null)
            sw.setRefreshing(false);
        if (txtEmpty != null)
            txtEmpty.setVisibility(View.VISIBLE);
        adapter.setEmptyData();
    }

    @Override
    public void onClick(View view, StreamOjb streamOjb, int position) {
        ((MainActivity) getActivity()).loadFragment(StreamDetailsFragment.newInstance(streamOjb.getId()), true);
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.message.equals("update_list_reject")) {
            presenter.getListReject(User.id);
        }

    }

}
