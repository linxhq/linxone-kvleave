package com.kvleave.manager.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.kvleave.R;
import com.kvleave.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ForApproverFragment extends BaseFragment {

    @BindView(R.id.sliding_tabs)
    TabLayout sliding_tabs;
    @BindView(R.id.vp)
    ViewPager vp;

    private PagerAdapter adapter;
    String type ="";




    @Override
    protected int idLayout() {
        return R.layout.fragment_init_tab_layout;
    }

    @Override
    protected int idBackButton() {
        return R.id.btnBack;
    }

    public static ForApproverFragment newInstance() {


        ForApproverFragment fragment = new ForApproverFragment();

        return fragment;
    }

    public static ForApproverFragment newInstance( String type) {
        ForApproverFragment fragment = new ForApproverFragment();
        fragment.type = type;
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(TabInProgressFragment.newInstance());
        fragments.add(TabApprovedFragment.newInstance());
        fragments.add(TabRejectFragment.newInstance());
        adapter = new com.kvleave.manager.adapter.PagerAdapter(getChildFragmentManager(),fragments,new String[]{"In Progress","Approved","Reject"});



        vp.setAdapter(adapter);
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        vp.setOffscreenPageLimit(limit);
        sliding_tabs.setupWithViewPager(vp);

        //getDataNotification();


    }
    private void getDataNotification()
    {

        if(!type.equals(""))
        {


            if(type.equals("send_request"))
            {

            }
        }
    }
}
