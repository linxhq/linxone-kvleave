package com.kvleave.notify.adapter;

import android.view.View;
import android.widget.TextView;

import com.kvleave.R;
import com.kvleave.base.BaseAdapter;
import com.kvleave.base.BaseViewHolder;
import com.kvleave.notify.models.DataNotify;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotifyAdapter extends BaseAdapter<DataNotify, NotifyAdapter.ViewHolder> {
    private OnClickListener onClickListener;

    public OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    protected int itemLayout() {
        return R.layout.item_notify;
    }

    @Override
    protected ViewHolder viewHoder(View view) {
        return new ViewHolder(view);
    }

     public class ViewHolder extends BaseViewHolder<DataNotify> {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtBody)
        TextView txtBody;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void bindData(View view, DataNotify dataNotify, int viewType, int position) {

            txtTitle.setText(dataNotify.getTitle());
            txtBody.setText(dataNotify.getBody());
            if(onClickListener!=null)
            {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClickListener.onClick(view,dataNotify,dataNotify.getType());
                    }
                });

            }
        }
    }
    public interface OnClickListener{
        void onClick(View view, DataNotify dataNotify, String type);

    }


}
