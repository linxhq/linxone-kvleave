package com.kvleave.notify.ui;

import com.kvleave.notify.models.DataNotify;

import java.util.List;

public interface  INotify {
    void responseData(List<DataNotify> notifies);
    void responseFail();
}
