package com.kvleave.notify.presenter;

import android.app.Activity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kvleave.helper.HttpRequest;
import com.kvleave.helper.StringUrl;
import com.kvleave.notify.models.Notify;
import com.kvleave.notify.ui.INotify;
import com.kvleave.notify.models.DataNotify;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class NotifyPresenter  {


    private Activity activity;
   private INotify iNotify;
    private String TAG = getClass().getSimpleName();

    private AsyncHttpClient client;
    private Gson gson = new Gson();


    public NotifyPresenter(Activity activity, INotify iNotify) {
        this.activity = activity;
        this.iNotify =iNotify;
        client = HttpRequest.getClient();

    }

    public void getListNotify()
    {

        RequestParams params = new RequestParams();
        params.put("id", com.kvleave.User.id);
        client.post(activity, StringUrl.urlNotify, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
                iNotify.responseFail();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iNotify.responseFail();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                iNotify.responseFail();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {

                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                        Notify data = gson.fromJson(response.toString(), Notify.class);
                        List<DataNotify> list = data.getData();
                        iNotify.responseData(list);
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    } else {

                        iNotify.responseFail();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    iNotify.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                iNotify.responseFail();
            }
        });
    }
}
