package com.kvleave.notify.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataNotify {

    @SerializedName("stream_id")
    @Expose
    private String StreamId;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;

    @SerializedName("isView")
    @Expose
    private Integer isView;


    public String getStreamId() {
        return StreamId;
    }

    public void setStreamId(String streamId) {
        StreamId = streamId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getIsView() {
        return isView;
    }

    public void setIsView(Integer isView) {
        this.isView = isView;
    }
}
