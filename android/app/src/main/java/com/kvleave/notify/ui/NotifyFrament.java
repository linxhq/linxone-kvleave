package com.kvleave.notify.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.kvleave.MainActivity;
import com.kvleave.MessageEvent;
import com.kvleave.R;
import com.kvleave.User;
import com.kvleave.activitystream.ui.StreamDetailsFragment;
import com.kvleave.activitystream.ui.StreamDetailsFragment_ViewBinding;
import com.kvleave.base.BaseFragment;
import com.kvleave.manager.ui.ForApproverFragment;
import com.kvleave.notify.adapter.NotifyAdapter;
import com.kvleave.notify.models.DataNotify;
import com.kvleave.notify.presenter.NotifyPresenter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NotifyFrament extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, INotify, NotifyAdapter.OnClickListener {
    @BindView(R.id.rcvNotify)
    RecyclerView rcvNotify;
    @BindView(R.id.sw)
    SwipeRefreshLayout sw;
    private List<DataNotify> dataNotifies;
    private NotifyAdapter adapter;
    private NotifyPresenter presenter;


    @Override
    protected int idLayout() {
        return R.layout.notify_frament;
    }

    @Override
    protected int idBackButton() {
        return R.id.btnBack;
    }

    public static NotifyFrament newInstance() {


        NotifyFrament fragment = new NotifyFrament();

        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new NotifyPresenter(getActivity(),this);
        presenter.getListNotify();
        if(sw!=null)
        sw.setOnRefreshListener(this);
        dataNotifies = new ArrayList<>();
        adapter = new NotifyAdapter();
        adapter.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcvNotify.setLayoutManager(linearLayoutManager);
        rcvNotify.setAdapter(adapter);

    }


    @Override
    public void onRefresh() {
        presenter.getListNotify();
    }

    @Override
    public void responseData(List<DataNotify> notifies) {
        if(sw!=null)
        sw.setRefreshing(false);
        adapter.setData(notifies);
    }

    @Override
    public void responseFail() {
        if(sw!=null)
        sw.setRefreshing(false);
    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.message.equals("update_activity")) {

        }else if(event.message.equals("notification"))
        {
            presenter.getListNotify();

        }

    }

    @Override
    public void onClick(View view, DataNotify dataNotify, String type) {
        switch (type)
        {
            case "send_request":
                ((MainActivity)getActivity()).loadFragment(ForApproverFragment.newInstance(),true);

                break;

            case "approved":
                ((MainActivity)getActivity()).loadFragment(StreamDetailsFragment.newInstance(dataNotify.getStreamId()),true);
                break;
            case "cc":
                ((MainActivity)getActivity()).loadFragment(StreamDetailsFragment.newInstance(dataNotify.getStreamId()),true);
                break;

        }
    }
}
