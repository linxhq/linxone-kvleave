package com.kvleave.notify.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kvleave.activitystream.model.StreamOjb;

import java.util.List;

public class Notify {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DataNotify> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataNotify> getData() {
        return data;
    }

    public void setData(List<DataNotify> data) {
        this.data = data;
    }
}
