package com.kvleave.signup.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jgabrielfreitas.core.BlurImageView;
import com.kvleave.R;
import com.kvleave.signup.presenter.SignUpControl;
import com.kvleave.signup.presenter.SignUpPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends AppCompatActivity implements SignUpControl {

    @BindView(R.id.imgBackGround)
    BlurImageView imgBackGround;
    @BindView(R.id.txtLogo)
    TextView txtLogo;
    @BindView(R.id.email)
    AutoCompleteTextView email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.rePassword)
    EditText rePassword;
    @BindView(R.id.username)
    EditText username;
    @BindView(R.id.email_sign_up_button)
    Button emailSignUpButton;
    @BindView(R.id.lnlLogin)
    LinearLayout lnlLogin;
    @BindView(R.id.txtBack)
    TextView txtBack;
    @BindView(R.id.email_login_form)
    RelativeLayout emailLoginForm;
    @BindView(R.id.prLogin)
    ProgressBar prLogin;
    private SignUpPresenter signUpPresenter;
    private String e, p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        signUpPresenter = new SignUpPresenter(this, this);
        setBG();


    }

    private void setBG() {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 4;
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.bg1, opts);
        imgBackGround.setImageBitmap(bm);
        imgBackGround.setBlur(7);
        Typeface tf = Typeface.createFromAsset(getAssets(),
                "hvd-neo.ttf");
        txtLogo.setTypeface(tf);
        txtLogo.setTextSize(70f);
    }

    @Override
    public void responseSignUp() {
        prLogin.setVisibility(View.GONE);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Notification");
        builder.setMessage("You have become a member, login now!");
        builder.setPositiveButton("Back to login", (dialog, which) -> {
            dialog.dismiss();
            Intent intent = new Intent();
            intent.putExtra("email", e);
            intent.putExtra("password", p);
            setResult(RESULT_OK, intent);
            finish();
        });
        builder.show();

    }

    @Override
    public void responseFail() {
        emailLoginForm.animate()
                .alpha(1f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                        prLogin.setVisibility(View.GONE);
                        emailLoginForm.setVisibility(View.VISIBLE);
                    }
                });
    }




    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @OnClick({R.id.email_sign_up_button, R.id.txtBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.email_sign_up_button:

                e = email.getText().toString();
                p = password.getText().toString();
                String rp = rePassword.getText().toString();
                String name = username.getText().toString();

                boolean cancel = false;
                View focusView = null;


                if (TextUtils.isEmpty(e)) {
                    email.setError(getString(R.string.error_field_required));
                    focusView = email;
                    cancel = true;
                } else if (!isEmailValid(e)) {
                    email.setError(getString(R.string.error_invalid_email));
                    focusView = email;
                    cancel = true;
                }

                // Check for a valid password, if the user entered one.
                if (TextUtils.isEmpty(p)) {
                    password.setError(getString(R.string.error_field_required));
                    focusView = password;
                    cancel = true;

                } else if (!isPasswordValid(p)) {
                    password.setError(getString(R.string.error_invalid_password));
                    focusView = password;
                    cancel = true;
                }

                if (!p.equals(rp)) {
                    rePassword.setError(getString(R.string.password_not_match));
                    focusView = rePassword;
                    cancel = true;
                }
                // Check for a valid email address.

                if (TextUtils.isEmpty(name)) {
                    username.setError(getString(R.string.error_field_required));
                    focusView = username;
                    cancel = true;
                }
                if (cancel) {
                    focusView.requestFocus();
                } else {
                    emailLoginForm.animate()
                            .alpha(0.0f)
                            .setDuration(500)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);

                                    prLogin.setVisibility(View.VISIBLE);
                                    emailLoginForm.setVisibility(View.GONE);
                                }
                            });

                    signUpPresenter.signUp(e, rp, name);


                }
                break;
            case R.id.txtBack:
                onBackPressed();
                break;
        }
    }
}
