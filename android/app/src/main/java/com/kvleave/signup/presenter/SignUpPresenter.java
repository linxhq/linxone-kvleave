package com.kvleave.signup.presenter;

import android.app.Activity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kvleave.helper.HttpRequest;
import com.kvleave.helper.StringUrl;
import com.kvleave.login.model.UserLogin;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/*
 * Created by hadt on 11/27/18.
 */
public class SignUpPresenter {
    private AsyncHttpClient client;
    private Activity activity;
    private Gson gson = new Gson();
    private SignUpControl control;

    public SignUpPresenter(Activity activity,SignUpControl control) {
        this.activity = activity;
        this.control = control;
    }

    public void signUp(String email, String password,String name) {
        client = HttpRequest.getClient();
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("password", password);
        params.put("username", name);
        client.post(activity, StringUrl.urlSignUp, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                        control.responseSignUp();
                    } else {

                        control.responseFail();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                control.responseFail();
            }
        });
    }
}
