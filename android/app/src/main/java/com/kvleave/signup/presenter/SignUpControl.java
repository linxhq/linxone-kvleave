package com.kvleave.signup.presenter;

import com.kvleave.login.model.UserLogin;

/*
 * Created by hadt on 11/27/18.
 */
public interface SignUpControl {
    void responseSignUp();
    void responseFail();
}
