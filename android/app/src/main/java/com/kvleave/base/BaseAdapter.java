package com.kvleave.base;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseAdapter<T,VH extends BaseViewHolder<T>> extends RecyclerView.Adapter<VH> {

    private List<T> list = new ArrayList<>();

    protected abstract int itemLayout();
    protected  abstract VH viewHoder(View view);

    public void setData(List<T> data) {
        if (this.list.size() > 0)

            this.list.clear();
        list.addAll(data);
        notifyDataSetChanged();

    }
    public void setEmptyData()
    {
        list.clear();
        notifyDataSetChanged();
    }




    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout(), parent, false);
        return viewHoder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        T t = list.get(position);
        holder.bindData(holder.itemView, t,holder.getItemViewType() , position);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}
