package com.kvleave.base;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

public  abstract class BaseListViewAdapter<T,V extends BaseListviewViewHolder<T>> extends BaseAdapter {


    private List<T> list = new ArrayList<>();

    public void setData(List<T> list) {
        if(this.list.size()>0)
            this.list.clear();
        this.list.addAll(list) ;

    }

    protected abstract int getViewId();

    protected abstract V viewHolder(View v);

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public T getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
            convertView = LayoutInflater.from(parent.getContext()).inflate(getViewId(),null);

        T t = list.get(position);

        V v = viewHolder(convertView);
        if(t!=null)
            v.binData(t,convertView,position);
        return convertView;
    }
}
