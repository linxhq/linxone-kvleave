package com.kvleave.base;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class BaseListviewViewHolder<T> {

    View itemView;
   public BaseListviewViewHolder(View itemView)
   {
       this.itemView =itemView;
   }
    protected abstract void binData(T t, View view, int position);
}
