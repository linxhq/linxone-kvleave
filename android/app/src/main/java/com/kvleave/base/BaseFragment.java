package com.kvleave.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {
    protected abstract int idLayout();
    protected abstract int idBackButton();

    Unbinder unbinder;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(idLayout(), container, false);
        unbinder = ButterKnife.bind(this, v);
        if(idBackButton()!=0)
        {
            View btn = v.findViewById(idBackButton());
            btn.setOnClickListener(v1 -> getActivity().onBackPressed());
        }

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    public  void setRecyclerView(RecyclerView recyclerView, boolean fixedsize, int cacheSize, boolean cacheEnable)
    {
        recyclerView.setHasFixedSize(fixedsize);
        recyclerView.setItemViewCacheSize(cacheSize);
        recyclerView.setDrawingCacheEnabled(cacheEnable);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
    }
}
