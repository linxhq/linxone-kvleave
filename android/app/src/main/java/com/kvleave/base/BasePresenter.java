package com.kvleave.base;

import android.app.Activity;

public interface BasePresenter<T> {
    void attach(Activity activity,T iView );
    void detach();
}
