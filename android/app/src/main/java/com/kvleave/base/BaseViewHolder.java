package com.kvleave.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;


 public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {

     
     protected abstract void bindData(View view, T t, int viewType, int position);

    public BaseViewHolder(View itemView) {
        super(itemView);


    }

}
