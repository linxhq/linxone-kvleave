package com.kvleave.login.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.jgabrielfreitas.core.BlurImageView;
import com.kvleave.MainActivity;
import com.kvleave.R;
import com.kvleave.User;
import com.kvleave.helper.KeyboardHelper;
import com.kvleave.helper.PrefUtils;
import com.kvleave.login.model.UserLogin;
import com.kvleave.login.presenter.LoginControl;
import com.kvleave.login.presenter.LoginPresenter;
import com.kvleave.signup.ui.SignupActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor>,LoginControl {


    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    @BindView(R.id.email)
    AutoCompleteTextView email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.email_sign_in_button)
    Button emailSignInButton;
    @BindView(R.id.email_login_form)
    RelativeLayout emailLoginForm;
//
//    @BindView(R.id.txtLogo)
//    TextView txtLogo;


    @BindView(R.id.prLogin)
    ProgressBar prLogin;
    @BindView(R.id.txtSignup)
    TextView txtSignup;
    @BindView(R.id.imgBackGround)
    BlurImageView imgBackGround;
    LoginPresenter presenter;
    private int REGISTER_RESULT_CODE=111;
    private  String token = null;


    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        //  setupWindowAnimations();
        ButterKnife.bind(this);
        presenter = new LoginPresenter(this,this);
        // Set up the login form.
        setBG();
        populateAutoComplete();





        password.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                KeyboardHelper.hideKeyBoard(LoginActivity.this);
                attemptLogin();

                return true;
            }
            return false;
        });



        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                KeyboardHelper.hideKeyBoard(LoginActivity.this);
                attemptLogin();
            }
        });
        txtSignup.setOnClickListener(v -> {

            startActivityForResult(new Intent(LoginActivity.this, SignupActivity.class),REGISTER_RESULT_CODE);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        });

    }


    private void setBG() {

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 4;
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.bg1, opts);
        imgBackGround.setImageBitmap(bm);
        imgBackGround.setBlur(7);



//        Typeface tf = Typeface.createFromAsset(getAssets(),
//                "hvd-neo.ttf");
//        txtLogo.setTypeface(tf);
//        txtLogo.setTextSize(90f);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(email, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        /*startActivity(new Intent(LoginActivity.this,MainActivity.class));
        finish();*/

        // Reset errors.
        email.setError(null);
        password.setError(null);

        String e = email.getText().toString();
        String p = password.getText().toString();
        pass =password.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(p)) {
            password.setError(getString(R.string.error_field_required));
            focusView = password;
            cancel = true;
        } else if (!isPasswordValid(p)) {
            password.setError(getString(R.string.error_invalid_password));
            focusView = password;
            cancel = true;
        }
        // Check for a valid email address.
        if (TextUtils.isEmpty(e)) {
            email.setError(getString(R.string.error_field_required));
            focusView = email;
            cancel = true;
        } else if (!isEmailValid(e)) {
            email.setError(getString(R.string.error_invalid_email));
            focusView = email;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            emailLoginForm.animate()
                    .alpha(0.0f)
                    .setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);

                            prLogin.setVisibility(View.VISIBLE);
                            emailLoginForm.setVisibility(View.GONE);
                        }
                    });



            if( PrefUtils.getTokenUser(this) == null )
            {
                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
                    token = task.getResult().getToken();
                    PrefUtils.storeTokenUser(this,token);
                    presenter.login(email.getText().toString(),password.getText().toString(),token);
                });
            }else {
                 token = PrefUtils.getTokenUser(this);
                presenter.login(email.getText().toString(),password.getText().toString(),token);
            }




        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }


    /**
     * Shows the progress UI and hides the login form.
     */


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        email.setAdapter(adapter);
    }
    String pass ="";
    @Override
    public void responseLogin(UserLogin userLogin) {

        User.username = userLogin.getData().getUsername();
        User.email = userLogin.getData().getEmail();
        User.id = userLogin.getData().getId();
        User.role =userLogin.getData().getRole();
        PrefUtils.storeAccount(this,  User.email,pass);
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void responseLogout() {

    }

    @Override
    public void responseFail() {
        emailLoginForm.animate()
                .alpha(1f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);

                        prLogin.setVisibility(View.GONE);
                        emailLoginForm.setVisibility(View.VISIBLE);

                    }
                });



    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == this.REGISTER_RESULT_CODE && resultCode ==RESULT_OK)
        {
            String e = data.getStringExtra("email");
            String p = data.getStringExtra("password");
            email.setText(e);
            password.setText(p);


        }
    }
}

