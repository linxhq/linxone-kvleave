package com.kvleave.login.presenter;

import android.app.Activity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kvleave.helper.HttpRequest;
import com.kvleave.helper.StringUrl;
import com.kvleave.login.model.UserLogin;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/*
 * Created by hadt on 11/27/18.
 */
public class LoginPresenter {
    private AsyncHttpClient client;
    private Activity activity;
    private Gson gson;
    private LoginControl control;

    public LoginPresenter(Activity activity, LoginControl control) {
        this.activity = activity;
        this.control = control;
    }

    public void login(String username, String password,String token) {
        gson = new Gson();
        client = HttpRequest.getClient();
        RequestParams params = new RequestParams();
        params.put("email", username);
        params.put("password", password);
        params.put("token", token);
        client.post(activity, StringUrl.urlLogin, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                        UserLogin userLogin = gson.fromJson(response.toString(), UserLogin.class);
                        control.responseLogin(userLogin);
                    } else {
                        control.responseFail();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    control.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                control.responseFail();
            }
        });
    }

    public void logout(String username) {
        gson = new Gson();
        client = HttpRequest.getClient();
        RequestParams params = new RequestParams();
        params.put("email", username);

        client.post(activity, StringUrl.urlLogout, params, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    boolean status = response.getBoolean("status");
                    String message = response.getString("message");
                    if (status) {
                      //  UserLogin userLogin = gson.fromJson(response.toString(), UserLogin.class);
                        control.responseLogout();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    } else {
                        control.responseFail();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    control.responseFail();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                control.responseFail();
            }
        });
    }
}
