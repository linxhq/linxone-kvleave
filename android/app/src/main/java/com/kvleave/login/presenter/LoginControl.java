package com.kvleave.login.presenter;

import com.kvleave.login.model.UserLogin;

/*
 * Created by hadt on 11/27/18.
 */
public interface LoginControl {
    void responseLogin(UserLogin userLogin);
    void responseLogout();
    void responseFail();
}
