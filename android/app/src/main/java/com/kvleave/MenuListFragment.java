package com.kvleave;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kvleave.base.BaseFragment;
import com.kvleave.helper.PrefUtils;
import com.kvleave.login.model.UserLogin;
import com.kvleave.login.presenter.LoginControl;
import com.kvleave.login.presenter.LoginPresenter;
import com.kvleave.login.ui.LoginActivity;
import com.kvleave.manager.ui.ForApproverFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class MenuListFragment extends BaseFragment implements LoginControl {

    @BindView(R.id.txtManage)
    TextView txtManage;
    @BindView(R.id.txtLogout)
    TextView txtLogout;
    @BindView(R.id.txtUsername)
    TextView txtUsername;

    private LoginPresenter presenter;
    private int num =0;
    private Activity mCon ;

    @Override
    protected int idLayout() {
        return R.layout.fragment_menu;
    }

    @Override
    protected int idBackButton() {
        return 0;
    }


    @Override
    public void onAttach(Activity context) {
        this.mCon = context;
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new LoginPresenter(getActivity(), this);
        if(User.role == 1)
            txtManage.setVisibility(View.GONE);
        txtUsername.setText(User.username);
    }

    @Override
    public void responseLogin(UserLogin userLogin) {

    }

    @Override
    public void responseLogout() {


        if(mCon != null) {
            PrefUtils.clearAll(mCon);
            Intent intent = new Intent(mCon, LoginActivity.class);
            startActivity(intent);
            mCon.finish();
        }
    }

    @Override
    public void responseFail() {


    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.message.equals("update_activity")) {

        } else if (event.message.equals("notification")) {

            num+=1;
            txtManage.setText("Manage +"+num);
        }


    }


    @OnClick({R.id.txtManage, R.id.txtLogout})
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.txtManage:
                ((MainActivity)getActivity()).closeDraw();
                txtManage.setText("Manage");
                ((MainActivity) getActivity()).loadFragment(ForApproverFragment.newInstance(), true);
                break;
            case R.id.txtLogout:
                ((MainActivity)getActivity()).closeDraw();
                presenter.logout(User.email);
                break;
        }
    }
}
